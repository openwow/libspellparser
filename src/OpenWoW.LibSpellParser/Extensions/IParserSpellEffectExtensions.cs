﻿using OpenWoW.Common;
using OpenWoW.LibSpellParser.Interfaces;

namespace OpenWoW.LibSpellParser
{
    public static class IParserSpellEffectExtensions
    {
        public static bool IsDamageOrHealingEffect(this IParserSpellEffect spellEffect)
        {
            return (
                // Aura type
                spellEffect.Aura == SpellEffectAura.PeriodicDamage ||
                spellEffect.Aura == SpellEffectAura.PeriodicHeal ||
                spellEffect.Aura == SpellEffectAura.PeriodicLeech ||
                spellEffect.Aura == SpellEffectAura.ProcTriggerDamage ||
                spellEffect.Aura == SpellEffectAura.SchoolAbsorb ||
                // Effect type
                spellEffect.Effect == SpellEffectEffect.Heal ||
                spellEffect.Effect == SpellEffectEffect.NormalizedWeaponDmg ||
                spellEffect.Effect == SpellEffectEffect.SchoolDamage ||
                spellEffect.Effect == SpellEffectEffect.WeaponDamage ||
                spellEffect.Effect == SpellEffectEffect.WeaponDamageNoschool ||
                false
            );
        }
    }
}
