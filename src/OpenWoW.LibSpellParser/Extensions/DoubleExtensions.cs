﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.LibSpellParser
{
    public static class DoubleExtensions
    {
        public static string ToTooltipNumber(this float val, int precision = 0) => ((double)val).ToTooltipNumber(precision);
        public static string ToTooltipNumber(this double val, int precision = 0)
        {
            return val.ToString($"N{ precision.ToString() }");
        }

        public static string ToTooltipNumberAuto(this double val, bool addCommas, int precision = 2)
        {
            // first, see if it could be rounded to exactly precision+1 places, then use that instead of precision
            string s = Math.Round(val, precision + 4).ToString("");

            if (s.Contains("."))
            {
                while (s.EndsWith("0"))
                {
                    s = s.Substring(0, s.Length - 1);
                }
                if (s.EndsWith("."))
                    s = s.Substring(0, s.Length - 1);
            }

            if (s.Length - s.IndexOf(".") == precision + 2)   // e.g., xx.yyy  01.345   6-2 = 4, 
            {
                return s;       // if precision specified was 2, has exactly 3 digits when zeroes were removed -- use it instead
            }


            s = Math.Round(val, precision).ToString("");
            //if (addCommas) s = AddCommas(s);

            if (s.Contains("."))
            {
                while (s.EndsWith("0"))
                {
                    s = s.Substring(0, s.Length - 1);
                }
                if (s.EndsWith("."))
                    s = s.Substring(0, s.Length - 1);
            }

            return s;
        }
    }
}
