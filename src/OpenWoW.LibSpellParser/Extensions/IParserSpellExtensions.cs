﻿using OpenWoW.Common;
using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.LibSpellParser
{
    public static class IParserSpellExtensions
    {
        public static IEnumerable<IParserSpell> GetFilteredAffected(this IParserSpell spell, IEnumerable<IParserSpell> affected, HashSet<int> specSpellIDs = null)
        {
            if (specSpellIDs == null)
            {
                specSpellIDs = new HashSet<int>();
            }

            // Class abilities need to limit the affected spells list to similar spells
            if (spell.Category == SpellCategory.ClassAbilities)
            {
                if (spell.SubSubCategory == (int)SpellClassAbilityCategory.Specialization)
                {
                    //var specSpellIDs = new HashSet<int>(ComplexDbcRelationships.Instance.GetSpellsByChrSpecializationID(spell.SubSubSubCategory).Select(x => x.ID));
                    //Console.WriteLine("SSI! spell={0} spells={1}", ID, String.Join(",", specSpellIDs));

                    affected = affected.Where(x =>
                        (
                            x.Category == SpellCategory.ClassAbilities &&
                            x.SubCategory == spell.SubCategory && // Class
                            (
                                // Specialization spells need to match this spec
                                (
                                    x.SubSubCategory == spell.SubSubCategory &&
                                    x.SubSubSubCategory == spell.SubSubSubCategory
                                ) ||
                                // WOW-649: multi-specialization spells also exist, ugh
                                specSpellIDs.Contains(x.InternalID) ||
                                // Class things that don't match the sub-sub-category (baseline, talents, etc)
                                x.SubSubCategory != spell.SubSubCategory
                            )
                        ) ||
                        (
                            x.Category == SpellCategory.ArtifactTraits &&
                            x.SubCategory == spell.SubCategory && // Class
                            x.SubSubCategory == spell.SubSubSubCategory // Spec
                        )
                    );
                }
                else
                {
                    affected = affected.Where(x =>
                        (
                            x.Category == SpellCategory.ClassAbilities &&
                            x.SubCategory == spell.SubCategory // Class
                        ) ||
                        (
                            x.Category == SpellCategory.ArtifactTraits &&
                            x.SubCategory == spell.SubCategory // Class
                        )
                    );
                }
            }
            return affected;
        }
    }
}
