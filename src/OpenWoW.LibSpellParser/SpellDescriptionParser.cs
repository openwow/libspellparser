﻿using OpenWoW.Common;
using OpenWoW.LibSpellParser.Implementation;
using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace OpenWoW.LibSpellParser
{
    internal class SpellDescriptionParser
    {
        private int LoopCounter;
        private IBaseSpellParser Parser;
        private SpellParserOptions Options;

        #region Regular expressions
        // This needs to match "|cF0F0F0F0blah" followed by another color (\c as a lookahead assertion so it isn't consumed),
        // color reset (\r), or end of string (\z)
        internal static readonly Regex colorRegex = new Regex(@"\|c[a-f0-9]{2}([a-f0-9]{6})(.*?)(?:(?=\|c)|\|r|\z)", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
        private static readonly Regex functionsRegex = new Regex(@"\$(?<Name>[a-z_]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
        private static readonly Regex externalVariablesRegex = new Regex(@"\$<([a-z0-9_]+)>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex singularPluralRegex = new Regex(@"(?<IsSingular>1 )?\$l(?<Singular>[^:]+):(?<Plural>[^;]+);", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
        private static readonly Regex singularPluralRegex2 = new Regex(@"\$(?<SpellID>[0-9]+)l(?<Singular>[^:]+):(?<Plural>[^;]+);", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex maleFemaleRegex = new Regex(@"\$g(?<Male>[^:]+):(?<Female>[^;]+);", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
        private static readonly Regex mathOperatorWithoutSpaces = new Regex(@"([/*\-+])", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex simplifyPercentages = new Regex(@"(?<OpeningParen>\(\[)?\$(?<VariableName>[a-z]+)(\s*\*\s*(?<Percentage>[0-9\.,]+))+(?<ClosingParen>\)\])?", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
        private static readonly Regex simplifyPercentages2 = new Regex(@"(?<OpeningParen>\(\[)?(?<Percentage>[0-9\.,]+)(\s*\*\s*\$(?<VariableName>[a-z]+))+(?<ClosingParen>\)\])?", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
        private static readonly Regex condRegex = new Regex(@"\$cond\(\$[a-z]+\(.+?\),(.+?),(.+?)\)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex condGtSpApRegex = new Regex(@"\$\{\$cond\(\$gt\(\$[AS]P[^,]*,\$[AS]P[^,]*\),([^,]+),([^,]+)\)([^\}]*)\}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex inlineIconRegex = new Regex(@"\|Tinterface\\(?:icons|pvpframe)\\(?<Name>[^:]+):(?<Size>\d+)[\d:-]*?\|t", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
        private static readonly Regex moneyIconRegex = new Regex(@"([0-9]+)\s*\|TINTERFACE\\MONEYFRAME\\UI-([a-z]+)ICON\.BLP:0\|t", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex currencyRegex = new Regex(@"\|Hcurrency:(?<CurrencyId>\d+)\|h(?<Text>.*?)\|h", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex minMaxDamageRegex = new Regex(@"(?<MinDamage>\${[a-z$\.\+\-/*0-9]+\}) to (?<MaxDamage>\${[a-z$\.\+\-/*0-9]+\})", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
        private static readonly Regex minMaxDamageRegex2 = new Regex(@"[?<MinDamage>\${[a-z$\.\+\-/*0-9]+\}] to [?<MaxDamage>\${[a-z$\.\+\-/*0-9]+\}]", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
        private static readonly Regex spacesRegex = new Regex(@"\s{2,}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex switchRegex = new Regex(@"\$@switch<(?<Value>\d+)>\[(?<One>[^\]]*?)\]\[(?<Two>[^\]]*?)\]", RegexOptions.Compiled);

        // $@spell
        private static readonly Regex spellAuraRegex = new Regex(@"\$?@spellaura([0-9]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex spellDescRegex = new Regex(@"\$?@spell?(?:des?c|id) ?([0-9]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex spellDescRegex2 = new Regex(@"\$?@([0-9]+)spelldesc", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex spellDescRegex3 = new Regex(@"\$?@([0-9]+)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex spellIconRegex = new Regex(@"\$?@spellicon([0-9]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex spellNameRegex = new Regex(@"\$?@spellname([0-9]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex spellTooltipRegex = new Regex(@"\$?@spelltooltip([0-9]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex garrAbilityRegex = new Regex(@"\$?@garrabdesc(\d+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex garrBuildingRegex = new Regex(@"\$?@garrbuilding([0-9]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private static Regex[] spellDescRegexes = new Regex[] { spellDescRegex, spellDescRegex2, spellDescRegex3 };

        // $gte, $lte as related to Player Level
        private static readonly Regex spellPlComparisonRegex = new Regex(@"\$(?<FunctionName>[g|l]te?)\(\s*(?<Arg1>[^,]+)\s*,\s*(?<Arg2>[^\)]+)\)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);

        // WOW-261
        private static readonly Regex deadlyThrowRegex = new Regex(@"(\$\?s84601.+)$", RegexOptions.Compiled);
        // % of weapon damage
        private static readonly Regex fixWeaponDamageRegex = new Regex(@"([\d\,\.]+\% of weapon damage) ((?:additional )?\S+ damage)", RegexOptions.Compiled);
        // $1234C - strange alternate cost
        private static readonly Regex fixAlternateCostRegex = new Regex(@"\$(\d+)C\b", RegexOptions.Compiled);
        // $1234X5 - strange alternate chain
        private static readonly Regex fixAlternateChainRegex = new Regex(@"\$(\d+)X(\d+)\b", RegexOptions.Compiled);
        // Secondary stat match
        private static readonly Regex secondaryStatRegex = new Regex(@"(\d+) (Critical Strike|Haste|Mastery|Versatility)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex secondaryStatRegex2 = new Regex(@"(Critical Strike|Haste|Mastery|Versatility) by (\d+)(\-\d+)?\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        // Spell conditionals?
        private static Regex conditionalRegex = new Regex(@"\?(\w)(\d+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        #endregion

        public SpellDescriptionParser(IBaseSpellParser parser, SpellParserOptions options)
        {
            Parser = parser;
            Options = options;
        }

        #region Parse functions
        public string Parse(IParserGarrAbility ability)
        {
            // Strip colors first
            string description = colorRegex.Replace(ability.Description, delegate (Match match)
            {
                return match.Groups[2].Value;
            });
            // Replace $a with $ga so that we use the correct effect value
            description = description.Replace("$a", "$ga");
            // Replace $m with $ga so that we use the correct effect value - should this be MinPoints instead?
            description = description.Replace("$m", "$ga");
            // Replace $h with $gd so that we use the correct duration value
            description = description.Replace("$h", "$gd");

            return InternalParse(null, description, null, Options);
        }

        public string Parse(IParserSpell spell, string description)
        {
            spell = spell ?? Parser.GetNewSpell();
            bool classItem = Options.ItemClass.HasValue && (description.Contains("?s") || description.Contains("?c"));

            try
            {
                LoopCounter++;
                if (LoopCounter > 10)
                {
                    return "[ERROR: sure looks like an infinite loop]";
                }

                Class spellClass = spell.RequiredClass;

                //If the item that this spell belongs to is limited to a certain class, then grab the spec spells for that class for multi-spec item effects
                if (classItem)
                {
                    spellClass = Options.ItemClass.Value;
                }

                // If a spell has no class, check for any related spells and see if they have a class instead
                IParserSpell relatedSpell = null;
                if (spellClass == Class.None)
                {
                    relatedSpell = spell.GetRelatedSpellWithClass();
                    if (relatedSpell != null)
                    {
                        spellClass = relatedSpell.RequiredClass;
                    }
                }

                if ((spellClass == 0 || !Options.ShowMultiSpec) && !classItem)
                {
                    // WOW-669: if a spec was provided, use the specialization spells as the known spells list. This fixes a specific case where
                    // an artifact trait spell is used for every class with a long if/elseif chain of aura checks.
                    HashSet<int> knownSpells = null;
                    if (Options.SpecializationID > 0)
                    {
                        var specialization = Parser.GetChrSpecialization(Options.SpecializationID);
                        if (specialization != null)
                        {
                            knownSpells = new HashSet<int>(specialization.SpecializationSpellIDs);
                        }
                    }

                    var newOptions = new SpellParserOptions(Options);
                    newOptions.ActiveTree = 0;
                    return InternalParse(spell, description, knownSpells, newOptions);

                    //return parse(spell, description, itemLevel, knownSpells, 0, false, null, itemSlot, doSubTooltips, itemRarity, playerLevel, parserLog: parserLog,
                    //    artifactRank: artifactRank, LoopCounter: LoopCounter);
                }

                IParserChrSpecialization[] specs = null;
                var spellSpecs = (relatedSpell ?? spell).ChrSpecializations;
                if (spellSpecs.Any())
                {
                    specs = spellSpecs.ToArray();
                }

                //if (spell.SubSubCategoryID == (int)SpellClassAbilitySubSubCategory.Specialization)
                //    specs = WowSpecializationSpell.GetAllBySpell(spell).Select(x => x.Category).Where(x => x != null).ToArray();

                /* If the spell is a class spell, we will have to check conditionals inside its description against specialization spells.
                 * We will generate 3 tooltips: 1 for each specialization. We will then diff them. */
                Dictionary<string /* Tooltip HTML */, string /* Specialization name(s) */> tooltipsBySpecialization = new Dictionary<string, string>();

                MatchCollection allConditionals = conditionalRegex.Matches(description);
                HashSet<int> conditionalSpells = new HashSet<int>();
                foreach (Match conditionalMatch in allConditionals)
                {
                    // [WOW-356] $?c1 type conditionals are a character specialization, not a spell
                    if (conditionalMatch.Groups[1].Value != "c")
                    {
                        conditionalSpells.Add(int.Parse(conditionalMatch.Groups[2].Value));
                    }
                }

                foreach (var specialization in Parser.GetAllChrSpecializationsByClass(spellClass))
                {
                    if (specs != null && !specs.Contains(specialization))
                    {
                        continue;
                    }

                    var specializationSpells = new HashSet<int>(specialization.SpecializationSpellIDs);
                    conditionalSpells.RemoveWhere(x => specializationSpells.Contains(x));

                    var newOptions = new SpellParserOptions(Options)
                    {
                        ActiveTree = specialization.OrderIndex,
                        SpecializationSpellIDs = specialization.SpecializationSpellPassiveInternalIDs,
                    };

                    string tooltip = InternalParse(spell, description, specializationSpells, newOptions);
                    //tooltip = parse(spell, description, itemLevel, specializationSpells, specialization.TreeNum, false, null, itemSlot, doSubTooltips, playerLevel: playerLevel,
                    //    parserLog: parserLog, artifactRank: artifactRank, LoopCounter: LoopCounter);

                    string specializationName = "";
                    if (Options.ShowIcons)
                    {
                        //specializationName = String.Format(@"<img src=""{0}"" alt="""" style=""vertical-align: middle"" /> {1}",
                        //specialization.GetIconUrl(IconSize.Small), specialization.Name);
                        specializationName = String.Format(@"<img src=""{1}"" style=""vertical-align: middle""> {0}",
                            specialization.Name, Parser.GetChrSpecializationIconURL(specialization));
                    }
                    else
                    {
                        specializationName = String.Format(@"{0}", specialization.Name);
                    }

                    if (tooltipsBySpecialization.ContainsKey(tooltip))
                    {
                        tooltipsBySpecialization[tooltip] += ", " + specializationName;
                    }
                    else
                    {
                        tooltipsBySpecialization[tooltip] = specializationName;
                    }
                }

                if (conditionalSpells.Any())
                {
                    var conditionalQueue = new Queue<int>(conditionalSpells);
                    var conditionalMap = new Dictionary<int, int>();

                    if (tooltipsBySpecialization.Count == 1 && !classItem)
                    {
                        tooltipsBySpecialization[tooltipsBySpecialization.First().Key] = "Normal";
                    }

                    if (tooltipsBySpecialization.Count == 1 && classItem)
                    {
                        tooltipsBySpecialization.Clear();
                    }

                    while (conditionalQueue.Count > 0)
                    {
                        int conditionalSpellID = conditionalQueue.Dequeue();

                        var conditionalSpell = Parser.GetSpellByExternalID(conditionalSpellID);
                        if (conditionalSpell?.Name == null)
                        {
                            continue;
                        }
                        if (conditionalSpell.Name.Contains("Glyph") || conditionalSpell.Name.StartsWith("Item "))
                        {
                            continue;
                        }

                        //LogDebug("conditional spell: {0} {1}", conditionalSpell.ExternalID, conditionalSpell.Name);

                        // [WOW-358] skip conditional spells that don't match specs we care about
                        if (specs != null)
                        {
                            int classID = specs.Select(x => (int)x.Class).Distinct().First();
                            var treeIDs = new HashSet<int>(specs.Select(x => x.OrderIndex + 1));

                            //LogDebug("- classID:{0} treeIDs:{1} cat:{2} subcat:{3} sub2cat:{4} sub3cat:{5}", classID, treeIDs.Select(x => x.ToString()).Join(","),
                            //    conditionalSpell.Category, conditionalSpell.SubCategory, conditionalSpell.SubSubCategory, conditionalSpell.SubSubSubCategory);

                            if (
                                conditionalSpell.Category == SpellCategory.ClassAbilities &&
                                (
                                    // Class ability but not the right class
                                    conditionalSpell.SubCategory != classID ||
                                    // Specialization but not the right spec
                                    (
                                        conditionalSpell.SubSubCategory == (int)SpellClassAbilityCategory.Specialization &&
                                        !treeIDs.Contains(conditionalSpell.SubSubCategory)
                                    )
                                )
                            )
                            {
                                //LogDebug("-- skipping after first check");
                                continue;
                            }
                        }

                        // [WOW-358] do more robust checking for spells that consist of $@spelldesc and friends
                        if (
                            specs != null &&
                            conditionalSpell.Category != SpellCategory.PvpTalents
                        )
                        {
                            var conditionalSpecs = conditionalSpell.ChrSpecializations.ToArray();

                            //LogDebug("-- trees: {0}", conditionalSpecs.Select(x => x.TreeNum.ToString()).Join(", "));
                            bool found = false;
                            foreach (var conditionalSpec in conditionalSpecs)
                            {
                                if (specs.Contains(conditionalSpec))
                                {
                                    found = true;
                                    break;
                                }
                            }

                            // If we didn't find a match, check for spelldesc redirects. Some spells have references
                            // to spells that consist entirely of $@spelldesc1234, and THOSE spells have the correct
                            // categorization.
                            if (!found)
                            {
                                foreach (Regex regex in spellDescRegexes)
                                {
                                    Match match = regex.Match(conditionalSpell.Description);
                                    if (match.Success)
                                    {
                                        int newID = int.Parse(match.Groups[1].Value);
                                        if (!conditionalSpells.Contains(newID))
                                        {
                                            conditionalSpells.Add(newID);
                                            conditionalQueue.Enqueue(newID);
                                            conditionalMap.Add(newID, conditionalSpellID);
                                        }
                                        break;
                                    }
                                }

                                // Skip now if this isn't a no-spec talent
                                if (!(
                                    conditionalSpell.Category == SpellCategory.ClassAbilities &&
                                    conditionalSpell.SubSubCategory == (int)SpellClassAbilityCategory.Talent &&
                                    conditionalSpecs.Length == 0
                                ))
                                {
                                    //LogDebug("-- skipping after second check");
                                    continue;
                                }
                            }
                        }

                        if (!conditionalMap.TryGetValue(conditionalSpellID, out int knownSpellID))
                        {
                            knownSpellID = conditionalSpellID;
                        }

                        var newOptions = new SpellParserOptions(Options);
                        newOptions.ActiveTree = 0;
                        string tooltip = InternalParse(spell, description, new HashSet<int> { knownSpellID }, newOptions);
                        //tooltip = parse(spell, description, itemLevel, new HashSet<int> { knownSpellID }, 0, false, null, itemSlot, doSubTooltips, playerLevel: playerLevel,
                        //    parserLog: parserLog, artifactRank: artifactRank, LoopCounter: LoopCounter);

                        string specializationName = "";
                        if (Options.ShowIcons)
                        {
                            specializationName = String.Format(@"<img src=""{2}"" style=""vertical-align: middle""> <a href=""/spells/{0}"">{1}</a>",
                                conditionalSpell.ExternalID, conditionalSpell.Name, Parser.GetSpellIconURL(conditionalSpell));
                        }
                        else
                        {
                            specializationName = String.Format(@"<a href=""/spells/{1}"">{0}</a>", conditionalSpell.Name, conditionalSpell.ExternalID);
                        }

                        if (tooltipsBySpecialization.ContainsKey(tooltip))
                        {
                            tooltipsBySpecialization[tooltip] += ", " + specializationName;
                        }
                        else
                        {
                            tooltipsBySpecialization[tooltip] = specializationName;
                        }
                    }
                }

                StringBuilder result = new StringBuilder();
                List<string> annoyingParts = new List<string>();

                foreach (var keyValue in tooltipsBySpecialization)
                {
                    if (keyValue.Key == "")
                    {
                        continue;
                    }

                    if (tooltipsBySpecialization.Count == 1 && !classItem)
                    {
                        annoyingParts.Add(keyValue.Key);
                    }
                    else
                    {
                        annoyingParts.Add(String.Format(
                            @"<span style=""color: White; font-size: 120%"">{0}:</span><br/><!--Spec:Start:{2}-->{1}<!--Spec:End-->",
                            keyValue.Value,
                            keyValue.Key,
                            keyValue.Value
                            //HtmlDiff.StripHtml(keyValue.Value)
                        ));
                    }
                }

                // Check for any perks that affect this spell if we're parsing the provided spell's description
                if (Options.ShowPerks && spell.Description.Equals(description, StringComparison.Ordinal))
                {
                    foreach (var affectingSpell in spell.AffectedByPerks)
                    {
                        StringBuilder temp = new StringBuilder();

                        var newOptions = new SpellParserOptions(Options);
                        newOptions.ActiveTree = 0;
                        string affectingTooltip = InternalParse(affectingSpell, affectingSpell.Description, null, newOptions);
                        //string affectingTooltip = parse(affectingSpell, affectingSpell.Description, itemLevel, null, 0, false, null, itemSlot, doSubTooltips, playerLevel: playerLevel,
                        //        parserLog: parserLog, artifactRank: artifactRank, LoopCounter: LoopCounter);

                        if (Options.ShowIcons)
                        {
                            temp.Append(String.Format(String.Format(@"<img src=""{0}"" style=""vertical-align: middle""> ",
                                Parser.GetSpellIconURL(affectingSpell))));
                        }

                        temp.Append(String.Format(@"<a href=""/spells/{0}"" class=""db-sublink"">{1}</a> (92+)<br/>{2}", affectingSpell.ExternalID, affectingSpell.Name, affectingTooltip));

                        annoyingParts.Add(temp.ToString());
                    }
                }

                result.Append(string.Join("<br/><br/>", annoyingParts));
                return result.ToString();
            }
            catch (ThreadAbortException e)
            {
                Thread.ResetAbort();
#if DEBUG
                string ret = "INTERNAL ERROR<br/><br/>Timeout while parsing description.";
                ret += "<br/><br/>" + e.ToString().Replace("\n", "<br/>");
                return ret;
#else
                //Parser.HandleException(e);
                return "Couldn't parse description.<br/>" + description;
#endif
            }
            catch (Exception e)
            {
#if DEBUG
                string ret = "INTERNAL ERROR<br/><br/>Couldn't parse description.";
                ret += "<br/><br/>" + e.ToString().Replace("\n", "<br/>");
                return ret;
#else
                Parser.HandleException(e);
                return "Couldn't parse description.<br/>" + description;
#endif
            }
        }
        #endregion

        #region Parser
        private string InternalParse(IParserSpell spell, string description, HashSet<int> knownSpells, SpellParserOptions options)
        {
            if (spell == null)
            {
                spell = Parser.GetNewSpell();
            }
            if (knownSpells == null)
            {
                knownSpells = new HashSet<int>();
            }

            //LogDebug("InternalParse: {0} known={1} description={2}", spell.ExternalID, String.Join(",", knownSpells.OrderBy(x => x)), description);

            var externalVariables = new Dictionary<string, string>();
            if (spell.DescriptionCode != null)
            {
                ParseDescriptionCodeStep(spell.DescriptionCode, ref externalVariables);
            }

            if (options.SpecializationID > 0)
            {
                options.AddLog(String.Format("Parsing for active tree {0}", options.SpecializationID), null);
            }
            options.AddLog("Initial", description);

            if (HardcodedData.SpecializationAuraSpellIDs.Contains(spell.ExternalID))
            {
                description = SpecializationAuraStep(description, spell);
                options.AddLog("Specialization aura", description);
            }

            description = NewLinesStep(description);
            options.AddLog("New lines", description);

            description = ColorStep(description);
            options.AddLog("Colors", description);

            if (spell.RequiredClass != 0)
            {
                description = HighToLowStep(description);
                options.AddLog("High to low", description);
            }

            description = ExternalVariablesStep(description, externalVariables);
            options.AddLog("Variables", description);

            description = FixCondGtStep(description);
            options.AddLog("Fix $cond($gt())", description);

            description = SimplifyPlayerLevelStep(description, options);
            options.AddLog("Simplify player level", description);

            description = FixAlternatesStep(description);
            options.AddLog("Fix alternates", description);

            description = IfsStep(description, knownSpells, options);
            options.AddLog("Ifs", description);

            description = ShortHandMathStep(description, spell, options);
            options.AddLog("Short hand math", description);

            description = ExplicitMathStep(description);
            options.AddLog("Explicit math", description);

            description = SimplifyPercentagesStep(description);
            options.AddLog("Siplify percentages", description);

            description = SwitchStep(description);
            options.AddLog("$switch", description);

            description = CondStep(description);
            options.AddLog("$cond", description);

            description = MiscStep(description, spell, options);
            options.AddLog("Misc", description);

            description = FunctionsStep(description);
            options.AddLog("Functions", description);

            // inline icons
            description = InlineIconsStep(description, options);
            options.AddLog("Inline icons", description);

            description = CurrenciesStep(description, options);
            options.AddLog("Currencies", description);

            if (options.ItemSlot > 0 && options.ItemLevel > 0)
            {
                description = CombatRatingMultiplierStep(description, options);
                options.AddLog("Combat rating multiplier", description);
            }

            // This has to come AFTER CombatRatingMultiplierStep or it will apply the multiplier twice.
            // This has to come BEFORE CleanupsStep which does horrible things to numbers.
            description = SpellLinksStep(description, spell, options);
            options.AddLog("Spell links", description);

            description = CleanupsStep(description);
            options.AddLog("Cleanups", description);

            description = ProcRppmStep(description, spell);
            options.AddLog("PPM", description);

            return description;
        }

        private void ParseDescriptionCodeStep(string spellDescriptionCode, ref Dictionary<string, string> externalVariables)
        {
            /////////////////////////////////////////////////////
            // Parse $<variables> (SpellDescriptionVariables.dbc)
            /////////////////////////////////////////////////////

            /////////////////////////////////////////////////////
            // FIXME: this is a hacky workaround for some parsing weirdness with Deadly Throw's SpellDescriptionCode. We change
            //        the second spell conditional to be the negative condition for the first spell conditional and everything
            //        is fine.  [WOW-261]
            /////////////////////////////////////////////////////
            /*if (spell.ExternalID == 26679)
            {
                spellDescriptionCode = deadlyThrowRegex.Replace(spellDescriptionCode, delegate (Match m) { return String.Format("[{0}]", m.Groups[1].ToString()); });
            }*/

            spellDescriptionCode = spellDescriptionCode.Replace("$versadmg", "0");
            spellDescriptionCode = spellDescriptionCode.Replace("$@versadmg", "0");

            int pos = 0;
            while ((pos = spellDescriptionCode.IndexOf("=${", pos)) != -1)
            {
                pos += 2;   // to the {
                pos = FindNestedBracket(spellDescriptionCode, pos, spellDescriptionCode[pos]);
                if (pos == -1)
                {
                    break;
                }

                pos++;
                if (pos >= spellDescriptionCode.Length)
                {
                    break;
                }

                if (spellDescriptionCode[pos] == '$')
                {  // }$ should be split to }\r\n$
                    spellDescriptionCode = spellDescriptionCode.Substring(0, pos) + "\r\n" + spellDescriptionCode.Substring(pos);
                }
            }

            string[] codeList = spellDescriptionCode.Split('\n');

            foreach (string code in codeList)
            {
                int equalPosition = code.IndexOf('=');
                if (equalPosition == -1)
                {
                    continue;
                }

                string variableName = code.Substring(1, equalPosition - 1);
                string variableValue = code.Substring(equalPosition + 1);

                //externalVariables[variableName] = parse(spell, variableValue, itemLevel, knownSpells, activeTreeId, true, externalVariables);

                // testing:
                // 33076:  [ 5,919 * Percent Health + 114.2% of Spell Power * Percent Health ]} 

                string s = "(" + variableValue + ")";
                s = s.Replace("\r", "");
                s = s.Replace("\n", "<br/>");
                externalVariables[variableName] = s;
            }
        }

        private string ColorStep(string description)
        {
            /////////////////////////////////////////////////////
            // Colors
            // |cFFFFAA00"You will fight the Horde and like it!"|r
            /////////////////////////////////////////////////////
            description = colorRegex.Replace(description, delegate (Match m)
            {
                return String.Format(@"<span style=""color: #{0}"">{1}</span>", m.Groups[1].Value, m.Groups[2].Value);
            });

            // Also fix stray |r's
            description = description.Replace("|r", "");

            return description;
        }

        private string SpecializationAuraStep(string description, IParserSpell spell)
        {
            /////////////////////////////////////////////////////
            // Spell Aura Handling (basically 'Hacks')
            // Handle the weird 'spec' spells that are missing descriptions (or have pointless ones)
            // e.g. SpellID 137020
            /////////////////////////////////////////////////////
            //description = ""; // Optionally we could blank out whatever text Blizzard puts here... but they could make these tooltips useful one day!

            foreach (var effect in spell.Effects)
            {
                // If the effect type isn't apply aura, it isn't a spec 'aura' any longer... so why would it be here?
                if (effect.Effect != SpellEffectEffect.ApplyAura)
                {
                    continue;
                }
                // If the effect isn't a modifier we don't want to show it in the tooltip
                if (effect.Aura != SpellEffectAura.AddFlatModifier && effect.Aura != SpellEffectAura.AddPctModifier)
                {
                    continue;
                }
                // If the effect isn't 0 (damage/healing) or 22 (periodic damage/healing) we don't want to show it in the tooltip
                if (effect.Misc1 != 0 && effect.Misc1 != 22)
                {
                    continue;
                }
                // If the effect value is 0 we don't want to show it in the tooltip
                if (effect.BasePoints == 0)
                {
                    continue;
                }
                // Skip empty Dummy effects
                if (effect.Aura == SpellEffectAura.Dummy && effect.Misc1 == 0 && effect.Misc2 == 0)
                {
                    continue;
                }

                // Get list of affected spells
                List<IParserSpell> effectAffected = spell.GetAffectedRelated(effect).ToList();

                //Console.WriteLine("SAS! spell={0} effect={1} affected={2}", spell.ExternalID, effect.Order, String.Join(", ", effectAffected.Select(x => String.Format("{0}/{1}", x.ExternalID, x.Name))));

                // If the affected spell list is empty, stop here for now
                // (we could potentially try to say this affects 'all' instead, but not sure of the reprocussions of such a change)
                if (effectAffected.Count == 0)
                {
                    continue;
                }

                // Build a single text string of spell names from list of names of affected spells
                List<String> affectedSpellsTextList = effectAffected
                    .Select(x => x.Name)
                    .Distinct()
                    .OrderBy(x => x)
                    .ToList();
                string affectedSpellsTextDesc = String.Join(affectedSpellsTextList.Count == 2 ? " and " : ", ", affectedSpellsTextList);

                if (!HardcodedData.SpellEffectAuraModifier.TryGetValue(effect.Misc1, out string effectText))
                {
                    effectText = String.Format("unknown effect #{0}", effect.Misc1);
                }

                // Supported aura types for 'make your description' functionality right now:
                // 107: Add Flat Modifier
                // 108: Add % Modifier
                string effectDesc = string.Format("<span class=\"db-title\">{0} {1} by {3}:</span>\n{2}",
                    (effect.BasePoints >= 0) ? "Increases" : "Decreases",
                    effectText.ToLowerInvariant(),
                    affectedSpellsTextDesc,
                    Math.Abs(effect.BasePoints).ToString() + ((effect.Aura == SpellEffectAura.AddPctModifier) ? "%" : ""));

                // Add double newlines to separate out effects (if needed)
                if (string.IsNullOrEmpty(description))
                {
                    description = effectDesc;
                }
                else
                {
                    description += "\n\n" + effectDesc;
                }
            }

#if DEBUG
            // For tooltip testing purposes
            //description += "\n\n" + new Random().Next(int.MinValue, int.MaxValue);
#endif

            return description;
        }

        private string NewLinesStep(string description)
        {
            /////////////////////////////////////////////////////
            // New lines
            /////////////////////////////////////////////////////

            description = description.Replace("\r", "");
            description = description.Replace("\n", "<br/>");

            description = description.Replace("$versadmg", "0");
            description = description.Replace("$@versadmg", "0");

            description = description.Replace("|n", "<br/>");
            description = description.Replace("|N", "<br/>");
            description = description.Replace(" ($@lootspec)", "");
            description = description.Replace(" $@lootspec", "");
            description = description.Replace("$@lootspec", "");

            return description;
        }

        private string HighToLowStep(string description)
        {
            /////////////////////////////////////////////////////
            // ${6*$<low>} to ${6*$<high>}
            // In class spells only, to make the tooltips easier to read, we keep only the max damage of the range.
            /////////////////////////////////////////////////////
            description = minMaxDamageRegex.Replace(description, delegate (Match m)
            {
                return m.Groups["MaxDamage"].ToString();
            });
            description = minMaxDamageRegex2.Replace(description, delegate (Match m)
            {
                return m.Groups["MaxDamage"].ToString();
            });

            return description;
        }

        private string ExternalVariablesStep(string description, Dictionary<string, string> externalVariables)
        {
            //if (externalVariables.Count != 0)     // some spells have $<vars> in the string but dont actually have the vars defined, so we can't exclude them here
            // also, converted to allow for recursive $<>'s
            bool foundNew = true;
            while (foundNew)
            {
                foundNew = false;

                description = externalVariablesRegex.Replace(description, delegate (Match m)
                {

                    if (externalVariables.TryGetValue(m.Groups[1].ToString(), out string replacement))
                    {
                        foundNew = true;
                        return "${" + replacement + "}";
                    }

                    // return "??";      // prefer to return the actual variable rather than "??"
                    return m.Groups[1].ToString();
                });
            }

            return description;
        }

        private string SimplifyPlayerLevelStep(string description, SpellParserOptions options)
        {
            // [old code] this one is extremely broken (probably doesn't handle recursive []'s) :
            //   (note: it may have worked better before, since parse() itself was recursive)
            // converts 22842:
            // "$?s54810[Increases healing received by $122307s1% for $122307d.][Instantly converts up to $s5 Rage into up to (${$max(($AP-2*$AGI)*$m2/100,$STA*$m3/100)*($?p123087[${1.1}][${1}]\r)}\r) health.]"
            // "Instantly converts up to $s5 Rage into up to (${$max(($AP-2*$AGI)*$m2/100,$STA*$m3/100)*($?p123087[${1.1}[${1}]\r)}\r) health.]"


            // Simplify gte, lte when related to player's level
            description = spellPlComparisonRegex.Replace(description, match =>
            {
                string arg1Str = match.Groups["Arg1"].ToString().ToLower();
                string arg2Str = match.Groups["Arg2"].ToString().ToLower();
                string functionName = match.Groups["FunctionName"].ToString().ToLower();
                string original = match.Groups["All"].ToString();
                int arg1, arg2;

                if (arg1Str == "$pl")
                {
                    if (!int.TryParse(arg2Str, out arg2))
                    {
                        return original;
                    }

                    arg1 = options.PlayerMaxLevel;
                }
                else if (arg2Str == "$pl")
                {
                    if (!int.TryParse(arg1Str, out arg1))
                    {
                        return original;
                    }

                    arg2 = options.PlayerMaxLevel;
                }
                else
                {
                    return original;
                }

                switch (match.Groups["FunctionName"].ToString().ToLower())
                {
                    case "gte":
                        return arg1 >= arg2 ? "1" : "0";
                    case "gt":
                        return arg1 > arg2 ? "1" : "0";
                    case "lte":
                        return arg1 <= arg2 ? "1" : "0";
                    case "lt":
                        return arg1 < arg2 ? "1" : "0";
                }

                return original;
            });

            return description;
        }

        private string FixAlternatesStep(string description)
        {
            // Weird alternate cost function
            /*description = fixAlternateCostRegex.Replace(description, delegate (Match m)
            {
                WowSpellPower power = spell.Powers.FirstOrDefault();
                return power != null ? power.Cost.ToTooltipNumber() : "0";
            });*/

            // Weird alternate chain function
            description = fixAlternateChainRegex.Replace(description, delegate (Match m)
            {
                int spellID = int.Parse(m.Groups[1].Value);
                int effectType = int.Parse(m.Groups[2].Value);
                var effectSpell = Parser.GetSpellByExternalID(spellID);
                if (effectSpell != null)
                {
                    var effect = effectSpell.Effects.Where(x => (int)x.Effect == effectType && x.DifficultyID == 0).FirstOrDefault();
                    if (effect != null)
                    {
                        return effect.ChainTargets.ToString();
                    }
                }
                return "0";
            });

            return description;
        }

        private string IfsStep(string description, HashSet<int> knownSpells, SpellParserOptions options)
        {
            /////////////////////////////////////////////////////
            // Ifs and negative ifs
            // $?s54354[has 54354][not], $?!s54354[not][has 54354], optionally $?(s123)
            /////////////////////////////////////////////////////

            while (true)
            {
                bool processed = false;

                for (int i = 0; i < description.Length - 4; i++)
                {
                    int offs = 0;

                    if (description[i] == '$' && description[i + 1] == '?')
                    {
                        offs = 2;
                    }

                    if (offs != 0)
                    {
                        // need to parse a list of $?if[true][false] with potentially extra ?s123[elseif] in between and [false] optional and occasionally junk between the ] and subsequent [

                        int starti = i;     // final string will be replaced starting here
                        int endi;
                        string ret = "";    // final string to replace with
                        bool found = false; // in the list of if/elseif/else's if we find one, we set this to true; if true, we just skip over the rest

                        i++;    // skip the '$'
                        while (true)
                        {
                            endi = i;
                            if (i >= description.Length)
                            {
                                break;  // need to have updated endi before we break the loop
                            }

                            if (description[i] == ' ')
                            { ret += " "; i++; }
                            if (description.Substring(i).StartsWith("<br/>"))
                            { ret += "<br/>"; i += 5; }   // some flexibility between ] and [

                            endi = i;                            // 2nd time
                            if (i >= description.Length)
                            {
                                break;  // need to have updated endi before we break the loop
                            }

                            if (description[i] != '?' && description[i] != '[')
                            {
                                break;
                            }


                            bool okToBreak = false;

                            if (description[i] == '?' && description[i + 1] == '$')
                            {
                                //i++;        // skip the '?' in "$?"
                                //endi = i;
                                okToBreak = true;
                                //break;      // fix bliz typo on 11328 -- $?$s1 ==> 15
                                // actually, these strings are sometimes valid -- $?$m1=0[...] -- so wait for the cond to be loaded and test if it's valid looking before breaking
                            }


                            int p = description.IndexOf('[', i);        // either [...] or ?xxx[...]
                            if (p != -1)
                            {
                                string cond = description.Substring(i, p - i);  // if cond is empty, this is an 'else' and it's automatically true
                                if (cond.Length > 0)
                                {
                                    if (okToBreak)
                                    {
                                        // probably don't need anything here, unless there's a bugged $?$ AND and [] following, which is usually valid?
                                    }

                                    if (cond[0] != '?')
                                    {
#if DEBUG
                                        System.Diagnostics.Debugger.Break();  // sanity check: todo: remove this!  :: must start with a '?' if it's a non-empty condition
#endif
                                    }
                                    else
                                    {
                                        cond = cond.Substring(1);   // remove the '?' from the condition
                                    }
                                }

                                int p2 = FindNestedBracket(description, p, description[p]);
                                int q = p2 + 1; // i advances here after this iteration

                                if (p2 == -1)
                                {
#if DEBUG
                                    System.Diagnostics.Debugger.Break();  // sanity check: todo: remove this!
#endif
                                    break;
                                }

                                // test cond :
                                // if true, replace with (p,p2)
                                // if found is true, dont replace anymore (just scan to end of elses)

                                if (!found && (cond.Equals("") || Condition.IsConditionTrue(cond.Replace("?", ""), knownSpells, options.ActiveTree)))
                                {
                                    if (p2 >= p)
                                    {
                                        ret += description.Substring(p + 1, p2 - (p + 1));
                                    }
                                    found = true;
                                }

                                i = p2 + 1;     // advance parser pointer to just after the ']'
                                processed = true;
                                //break;  // out of the for(i) loop
                            }
                            else
                            {
                                if (okToBreak)
                                {
                                    i++;        // skip the '?' in "$?"
                                    endi = i;
                                    break;   // handled bug case, so clean break
                                }

#if DEBUG
                                System.Diagnostics.Debugger.Break();  // sanity check: todo: remove this!
#endif
                                break;
                            }

                        }

                        description = description.Substring(0, starti) + ret + description.Substring(endi);
                    }
                }

                if (!processed)
                {
                    break;
                }
            }

            return description;
        }

        private string ShortHandMathStep(string description, IParserSpell spell, SpellParserOptions options)
        {
            /////////////////////////////////////////////////////
            // Variable + short hand math
            // Components:
            // Short hand math, optional ($/10;s1 == $s1 / 10) -- Regex names: MathOperation & MathNumber (both or neither exist)
            // External spell id, optional ($133s1 == $s1 of spell 133) -- Regex name: SpellID
            // Name ($max, max == name) -- Regex name: Name
            // Effect id, used only for some variables ($s1, $s of effect 1) -- Regex name: EffectID
            //
            // IMPORTANT!!!! This function _MUST_ always return a number.

            // rules:
            //   $      <-- initiates parse
            //   /123;  <-- math op must be op number ; <-- semicolon must always be present or else the math op is instead ignored and the number is treated as the ref spellid
            //   123    <-- spellid
            //   s      <-- name <-- all letters a-zA-z read in
            //   123    <-- effectid <-- optionally all numbers read in 0-9
            // then conversion proceeds

            int insideMathStart = -1;
            int insideMathEnd = -1;
            for (int i = 0; i < description.Length - 1; i++)
            {
                char c = description[i];
                char cnext = description[i + 1];

                if (i >= insideMathEnd)
                {
                    insideMathEnd = insideMathStart = -1;
                }

                if (insideMathStart == -1 && c == '$' && cnext == '{')
                {
                    insideMathStart = i + 1;
                    insideMathEnd = FindNestedBracket(description, insideMathStart, '{');
#if DEBUG
                    if (insideMathEnd == -1) System.Diagnostics.Debugger.Break();
#endif
                }
                else if (c == '$')
                {
                    char op = (char)0;
                    int mathNumber = 0;
                    int spellid = 0;
                    string name = "";
                    int effectId = 0;


                    VariableParseEnum state = VariableParseEnum.Nothing;

                    int j = i + 1;
                    while (true)
                    {
                        if (state == VariableParseEnum.Finished)
                        {
                            break;
                        }

                        char ch = (j < description.Length) ? description[j] : (char)0;

                        switch (state)
                        {
                            case VariableParseEnum.Nothing:
                                if ("+-*/".Contains(ch))
                                {
                                    op = description[j];
                                    state = VariableParseEnum.MathNumber;
                                    j++;
                                }
                                else if (ch >= '0' && ch <= '9')
                                {
                                    state = VariableParseEnum.MathNumber;
                                }
                                else if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
                                {
                                    state = VariableParseEnum.Name;
                                }
                                else
                                {
                                    state = VariableParseEnum.Finished;
                                    break;  // some other code, not applicable to us
                                }
                                break;

                            case VariableParseEnum.MathNumber:
                                if (ch >= '0' && ch <= '9')
                                {
                                    mathNumber = mathNumber * 10 + (ch - '0');
                                    j++;
                                }
                                else if (ch == ';')
                                {
                                    state = VariableParseEnum.SpellID;
                                    j++;
                                }
                                else
                                {
                                    spellid = mathNumber;
                                    mathNumber = 0;
                                    op = (char)0;
                                    state = VariableParseEnum.Name;
                                }
                                break;

                            case VariableParseEnum.SpellID:
                                if (ch >= '0' && ch <= '9')
                                {
                                    spellid = spellid * 10 + (ch - '0');
                                    j++;
                                }
                                else
                                {
                                    state = VariableParseEnum.Name;
                                }
                                break;

                            case VariableParseEnum.Name:
                                if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
                                {
                                    name += ch;
                                    j++;
                                }
                                else
                                {
                                    state = VariableParseEnum.EffectID;
                                }
                                break;

                            case VariableParseEnum.EffectID:
                                if (ch >= '0' && ch <= '9')
                                {
                                    effectId = effectId * 10 + (ch - '0');
                                    j++;
                                }
                                else
                                {
                                    state = VariableParseEnum.Finished;
                                }
                                break;

                        }

                        if (ch == 0)
                        {
                            break;
                        }
                    }

                    if (effectId > 0)
                    {
                        effectId--;
                    }

                    MathInfo math = new MathInfo();
                    IParserSpell externalSpell = null;
                    string ret = null;

                    if (op != (char)0)
                    {
                        math.Parse(op.ToString(), mathNumber.ToString());
                    }

                    if (spellid != 0)
                    {
                        externalSpell = Parser.GetSpellByExternalID(spellid);
                        if (externalSpell == null)
                        {
                            ret = "0";
                        }
                    }

                    if (ret == null)
                    {
                        bool insideMath = (insideMathStart == -1) ? false : true;
                        if (VariableHandlers.variableHandlers.ContainsKey(name))
                        {
                            ret = VariableHandlers.variableHandlers[name](Parser, externalSpell ?? spell, spell, effectId, insideMath, true, math, options);
                        }
                    }

                    if (ret != null)
                    {
                        description = description.Substring(0, i) + ret + description.Substring(j);
                        // resumed at j, started at i, so mathend also adjusts by -j +i
                        if (insideMathStart != -1)
                        {
                            insideMathEnd = insideMathEnd + ret.Length + (i - j);   // (i-j) is old length
                        }

                        i = i + ret.Length - 1;
                    }
                    else
                    {
                        i = j - 1;  // no change to description
                    }
                }
            }

            return description;
        }

        private string ExplicitMathStep(string description)
        {
            /////////////////////////////////////////////////////
            // Parse explicit math
            // Restores ${$m2/5} mana per second.
            /////////////////////////////////////////////////////

            Calc mathEvaluator = new Calc();

            // todo: might want to preprocess this to convert ${}'s into ()'s instead, so that the inner symbolic parser can use it straight away?

            while (true)
            {
                int extraSkip = 0;
                int p = description.IndexOf("${");
                if (p == -1)
                {
                    break;
                }

                int p2 = FindNestedBracket(description, p + 1, '{');
#if DEBUG
                if (p2 == -1) System.Diagnostics.Debugger.Break();
#endif

                string formula = description.Substring(p + 2, p2 - (p + 2));

                int overridePrecision = -1;     // auto
                if ((description.Length >= p2 + 3) && description[p2 + 1] == '.')
                {
                    char cp = description[p2 + 2];
                    if (cp >= '0' && cp <= '9')
                    {
                        overridePrecision = Convert.ToInt32(cp.ToString());
                        extraSkip = 2;
                    }
                }



                // Temporarily remove "% of weapon damage" to make math work properly
                string appendLater = null;
                if (formula.EndsWith("% of weapon damage"))
                {
                    appendLater = "% of weapon damage";
                    formula = formula.Replace("% of weapon damage", "");
                }

                formula = mathEvaluator.Evaluate(formula, false, false, out bool success, overridePrecision);

                // Re-add "% of weapon damage"
                if (appendLater != null)
                {
                    formula += appendLater;
                }

                if (success)
                {
                    description = description.Substring(0, p) + formula + description.Substring(p2 + 1 + extraSkip);
                }
                else
                {
                    /* Add spaces if not present.. Example: ${5*5} becomes (5 * 5) */
                    formula = mathOperatorWithoutSpaces.Replace(formula, delegate (Match m2)
                    {
                        string mathOperator = m2.Groups[1].ToString();

                        if (formula.Substring(m2.Index + 1).ToLower().StartsWith("handed"))
                        {
                            // don't change this one, it's probably a string containing One-Handed, 1-handed, Two-handed, etc
                            return mathOperator;
                        }
                        else
                        {
                            return " " + mathOperator + " ";
                        }
                    });

                    description = description.Substring(0, p) + "[ " + formula + " ]" + description.Substring(p2 + 1);
                }
            }

            return description;
        }

        private string SimplifyPercentagesStep(string description)
        {
            description = simplifyPercentages.Replace(description, SimplifyPercentages);
            description = simplifyPercentages2.Replace(description, SimplifyPercentages);
            return description;
        }

        private string SwitchStep(string description)
        {
            /////////////////////////////////////////////////////
            // $@switch<n>[1][2] - chooses 1 or 2 based on the value of n
            /////////////////////////////////////////////////////
            description = switchRegex.Replace(description, delegate (Match m)
            {
                if (int.TryParse(m.Groups["Value"].Value, out int n))
                {
                    if (n == 1)
                    {
                        return m.Groups["One"].Value;
                    }
                    else if (n == 2)
                    {
                        return m.Groups["Two"].Value;
                    }
                }

                return "ERROR";
            });
            return description;
        }

        private string FixCondGtStep(string description)
        {
            /////////////////////////////////////////////////////
            // Replace $cond(gt($SP,$AP),A,B) with MAX(A, B)
            // $cond($gt(...), "A", "B") = A or B
            /////////////////////////////////////////////////////
            description = condGtSpApRegex.Replace(description, delegate (Match m)
            {
                string extra = m.Groups[3].ToString();
                return $"[ Greater of {{{ m.Groups[1].ToString() }{ extra }}} or {{{ m.Groups[2].ToString() }{ extra }}} ]";
            });
            return description;
        }

        private string CondStep(string description)
        {
            /////////////////////////////////////////////////////
            // Parse $cond()
            // $cond($gt(...), "A", "B") = A or B
            /////////////////////////////////////////////////////
            description = condRegex.Replace(description, delegate (Match m)
            {
                string one = m.Groups[1].ToString();
                string two = m.Groups[2].ToString();

                if (String.IsNullOrEmpty(one))
                {
                    return two;
                }

                if (String.IsNullOrEmpty(two))
                {
                    return one;
                }

                return String.Format("({0} or {1})", one, two);
            });
            return description;
        }

        private string MiscStep(string description, IParserSpell spell, SpellParserOptions options)
        {
            /////////////////////////////////////////////////////
            // Remove unneeded parenthesis.
            // Example: ((20 + ((2 + 1) * 2) + 1)) = (20 + ((2 + 1) * 2) + 1)
            /////////////////////////////////////////////////////
            description = RemoveUnneededParenthesis(description);


            /////////////////////////////////////////////////////
            // Male / female
            // $ghis:her;
            /////////////////////////////////////////////////////
            description = maleFemaleRegex.Replace(description, delegate (Match m)
            {
                return m.Groups["Male"].ToString(); // Males are genetically superior to females
            });


            /////////////////////////////////////////////////////
            // Singular / Plural
            // 5 $lloaf:loaves; of bread
            // 1 $lBleak Worg:Bleak Worgs;
            /////////////////////////////////////////////////////
            description = singularPluralRegex.Replace(description, delegate (Match m)
            {
                if (m.Groups["IsSingular"].Success)
                {
                    return m.Groups["IsSingular"].ToString() + m.Groups["Singular"].ToString();
                }

                return m.Groups["Plural"].ToString();
            });

            // this one handles $12345lsingular:plural; that the previous one would miss
            description = singularPluralRegex2.Replace(description, delegate (Match m)
            {
                MathInfo math = new MathInfo();
                IParserSpell externalSpell = null;
                int effectId = 0;

                if (m.Groups["SpellID"].Success)
                {
                    externalSpell = Parser.GetSpellByExternalID(Int32.Parse(m.Groups["SpellID"].Value));

                    string countStr = VariableHandlers.variableHandlers["s"](Parser, externalSpell ?? spell, spell, effectId, false, true, math, options);

                    if (countStr == "1")
                    {
                        return m.Groups["Singular"].ToString();
                    }
                }

                return m.Groups["Plural"].ToString();
            });

            // Fix "n% of weapon damage SCHOOL damage" to be "(n% of weapon damage) SCHOOl damage"
            description = fixWeaponDamageRegex.Replace(description, delegate (Match m)
            {
                return String.Format("({0}) {1}", m.Groups[1].Value, m.Groups[2].Value);
            });

            return description;
        }

        private string SpellLinksStep(string description, IParserSpell spell, SpellParserOptions options)
        {
            /////////////////////////////////////////////////////
            // Spell Links
            // $@spellname119716 or @spellname119716
            // $@spelldesc119716 or @spelldesc119716
            // $@spellicon
            // $@spellaura
            // $@spelltooltip
            /////////////////////////////////////////////////////
            description = spellNameRegex.Replace(description, delegate (Match m)
            {
                int spellID = Int32.Parse(m.Groups[1].ToString());
                var spell2 = Parser.GetSpellByExternalID(spellID);

                return String.Format(@"<span style=""color: White"">{0}</span>", spell2?.Name ?? "Spell #" + spellID);
            });

            description = spellDescRegex.Replace(description, delegate (Match m)
            {
                int spellID = Int32.Parse(m.Groups[1].ToString());
                if (spellID == spell.ExternalID)
                {
                    return "";
                }
                var spell2 = Parser.GetSpellByExternalID(spellID);

                if (spell2 == null)
                {
                    return "";
                }

                var newOptions = new SpellParserOptions(options);
                newOptions.ShowPerks = false;
                return Parse(spell2, spell2.Description);
            });

            description = spellDescRegex2.Replace(description, delegate (Match m)
            {
                int spellID = Int32.Parse(m.Groups[1].ToString());
                if (spellID == spell.ExternalID)
                {
                    return "";
                }

                var spell2 = Parser.GetSpellByExternalID(spellID);

                if (spell2 == null)
                {
                    return "";
                }

                return Parse(spell2, spell2.Description);
            });

            description = spellIconRegex.Replace(description, delegate (Match m)
            {
                int spellId = Int32.Parse(m.Groups[1].ToString());
                if (spellId == spell.ExternalID || options.RunningDiffs)
                {
                    return "";
                }

                return Parser.GetSpellIconHtml(spell);
            });

            description = spellAuraRegex.Replace(description, delegate (Match m)
            {
                int spellID = Int32.Parse(m.Groups[1].ToString());
                if (spellID == spell.ExternalID)
                {
                    return "";
                }

                var spell2 = Parser.GetSpellByExternalID(spellID);
                if (spell2 == null)
                {
                    return "";
                }

                return Parse(spell2, spell2.Description);
            });

            description = garrAbilityRegex.Replace(description, delegate (Match m)
            {
                if (Int32.TryParse(m.Groups[1].Value, out int abilityID))
                {
                    var ability = Parser.GetGarrisonAbility(abilityID);
                    if (ability != null)
                    {
                        return Parse(ability);
                    }
                }
                return "??";
            });

            description = garrBuildingRegex.Replace(description, delegate (Match m)
            {
                int buildingID = Int32.Parse(m.Groups[1].ToString());
                var building = Parser.GetGarrisonBuilding(buildingID);
                if (building?.Name == null)
                {
                    return "";
                }

                StringBuilder ret = new StringBuilder();

                for (int garrLevel = 1; garrLevel <= 3; garrLevel++)
                {
                    if (garrLevel > 1)
                    {
                        var tempBuilding = Parser.GetGarrisonBuildingByNameAndLevel(building.Name, garrLevel);
                        if (tempBuilding?.Name != null)
                        {
                            building = tempBuilding;
                        }
                    }
                    ret.AppendLine("<dd class=\"yellow\">Item Level <span class=\"j-item-level\">" + garrLevel + "</span></dd>");
                    ret.AppendLine("<dd>" + building.Tooltip + (garrLevel == 3 ? Environment.NewLine : "") + "</dd>");
                }

                return ret.ToString();
            });

            description = spellTooltipRegex.Replace(description, delegate (Match m)
            {
                int spellId = Int32.Parse(m.Groups[1].ToString());
                if (spellId == spell.ExternalID)
                {
                    return "";
                }

                var spell2 = Parser.GetSpellByExternalID(spellId);
                if (spell2 == null || !options.DoSubTooltips)
                {
                    return "";
                }

                StringBuilder ret = new StringBuilder();

                // if running diffs, renderIcons = false because we cant diff icons that have different paths
                // some other problems also exist when using RenderPartialAsString (System.Exception: View could not be found: ~/Views/Shared/SpellTooltipBody.ascx)

                if (options.RunningDiffs)
                {
                    ret.Append(Parse(spell2, spell2.Description));
                }
                else
                {
                    ret.Append(@"<div class=""db-subtooltip"">");
                    ret.Append(Parser.RenderSpellTooltip(spell2));
                    ret.Append("</div>");
                }

                return ret.ToString();
            });

            // must be at end of all other $@1234's, as it implicitly assumes spelldesc is wanted

            description = spellDescRegex3.Replace(description, delegate (Match m)
            {
                int spellId = Int32.Parse(m.Groups[1].ToString());
                if (spellId == spell.ExternalID)
                {
                    return "";
                }

                var spell2 = Parser.GetSpellByExternalID(spellId);
                if (spell2 == null)
                {
                    return "";
                }

                return Parse(spell2, spell2.Description);
            });

            return description;
        }

        private string FunctionsStep(string description)
        {
            /////////////////////////////////////////////////////
            // Functions (such as $max, $min, but also $sp, $ap, etc)
            /////////////////////////////////////////////////////
            description = functionsRegex.Replace(description, delegate (Match m)
            {
                string name = m.Groups["Name"].Value;
                if (VariableHandlers.variableTextReplaces.TryGetValue(name, out string value))
                {
                    return value;
                }
                else
                {
                    return name;
                }
            });

            return description;
        }

        private string InlineIconsStep(string description, SpellParserOptions options)
        {
            /////////////////////////////////////////////////////
            // Inline images, such as: |Tinterface\icons\ability_warrior_warcry.blp:24|t 
            /////////////////////////////////////////////////////
            description = inlineIconRegex.Replace(description, delegate (Match m)
            {
                if (options.RunningDiffs)
                {
                    return "";
                }

                string name = m.Groups["Name"].ToString().Replace(".blp", "");
                return Parser.GetIconHtml(name);
            });

            description = moneyIconRegex.Replace(description, delegate (Match m)
            {
                string amount = m.Groups[1].ToString();
                string name = m.Groups[2].ToString().ToLower();

                return String.Format(@"<span class=""money""><span class=""money-{0}"">{1}</span></span>", name, amount);
            });

            return description;
        }

        private string CurrenciesStep(string description, SpellParserOptions options)
        {
            /////////////////////////////////////////////////////
            // Currency links, such as: |Hcurrency:1166|h|TInterface\Icons\pvecurrency-justice.BLP:0|t|h
            /////////////////////////////////////////////////////
            description = currencyRegex.Replace(description, delegate (Match m)
            {
                if (options.RunningDiffs)
                {
                    return "";
                }

                int currencyID = int.Parse(m.Groups["CurrencyId"].ToString());
                string text = m.Groups["Text"].ToString();
                return Parser.GetCurrencyLink(currencyID, text);
            });
            return description;
        }

        private string CombatRatingMultiplierStep(string description, SpellParserOptions options)
        {
            /////////////////////////////////////////////////////
            // Apply combat rating multipliers to secondary stats :(
            /////////////////////////////////////////////////////

            int itemLevel = options.ItemLevel.Value;
            int itemSlot = options.ItemSlot.Value;

            // (n) (stat)
            description = secondaryStatRegex.Replace(description, delegate (Match m)
            {
                if (int.TryParse(m.Groups[1].ToString(), out int replace))
                {
                    return String.Format("{0} {1}", Parser.GetScaledSecondaryStatValue(replace, itemSlot, itemLevel), m.Groups[2].ToString());
                }
                else
                {
                    return String.Format("{0} {1} [ERROR]", m.Groups[1], m.Groups[2].ToString());
                }
            });

            // (stat) by (n)
            description = secondaryStatRegex2.Replace(description, delegate (Match m)
            {
                if (int.TryParse(m.Groups[2].ToString(), out int replace1))
                {
                    int value1 = Parser.GetScaledSecondaryStatValue(replace1, itemSlot, itemLevel);
                    string valueString = value1.ToString();

                    // WOW-715: handle value ranges
                    if (m.Groups.Count == 4)
                    {
                        if (int.TryParse(m.Groups[3].ToString().Replace("-", ""), out int replace2))
                        {
                            int value2 = Parser.GetScaledSecondaryStatValue(replace2, options.ItemSlot.Value, options.ItemLevel.Value);
                            valueString = String.Format("{0}-{1}", value1, value2);
                        }
                    }

                    return String.Format("{0} by {1}", m.Groups[1].ToString(), valueString);
                }
                else
                {
                    return String.Format("{0} by {1} [ERROR]", m.Groups[1].ToString(), m.Groups[2].ToString());
                }
            });

            return description;
        }

        private string CleanupsStep(string description)
        {
            /////////////////////////////////////////////////////
            // Removes extra spaces
            /////////////////////////////////////////////////////
            description = spacesRegex.Replace(description, delegate (Match m)
            { return " "; });



            //if (!recursiveCall)
            //{
            description = AddCommas(description);
            //}

            description = ConvertInsideBrackets(description);

            description = description.Replace(")-(", ") to (");
            description = description.Replace("]-[", "] to [");


            for (int i = 0; i < description.Length;)
            {
                if (description[i] != '[')
                {
                    i++;
                }
                else
                {
                    int j = FindNestedBracket(description, i, '[');

                    if (j != -1 && j > i + 2 && description[i + 1] == ' ' && description[j - 1] == ' ')     // valid and not just a simple [] or [ ]
                    {
                        string ret = description.Substring(0, i + 1);       // get the front including the '['
                        i++;    // skip the '['
                        ret += "&nbsp;";
                        i++;

                        ret += description.Substring(i, j - 1 - i);     // get the middle
                        i = j - 1;                                      // skip to the space before the closing ']'

                        ret += "&nbsp;";
                        i++;

                        ret += description.Substring(j);

                        i = j + 1 + (6 - 1) * 2;    // converting two ' ' => "&nbsp;" and the standard j+1 to get to after the ']'

                        description = ret;
                    }
                    else
                    {
                        i++;
                    }
                }
            }

            // Fix "damage damage"
            description = description.Replace("damage damage", "damage");

            return description;
        }

        private string ProcRppmStep(string description, IParserSpell spell)
        {
            description = description.Replace("procrppm", VariableHandlers.HandleProcPerMin(spell));
            return description;
        }
        #endregion

        #region Utility functions
        // add commas to numbers in a string
        // do a quick/simple skip over html tags (wont handle all conditions)
        private static string AddCommas(string s)
        {
            string result = "";

            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == '<')
                {
                    for (; i < s.Length; i++)
                    {
                        result += s[i];
                        if (s[i] == '>')
                        {
                            break;
                        }
                    }
                }
                else if (s[i] == '.')   // if floating point number, never add commas until we're done with this number
                {
                    result += s[i++];

                    for (; i < s.Length; i++)
                    {
                        if (!(s[i] >= '0' && s[i] <= '9'))
                        {
                            break;
                        }

                        result += s[i];
                    }
                    i--;
                }
                else if (s[i] >= '0' && s[i] <= '9')
                {
                    int numStart = i;

                    for (; i < s.Length; i++)
                    {
                        if (!(s[i] >= '0' && s[i] <= '9'))
                        {
                            break;
                        }
                    }

                    // number from [numStart,i)

                    int commaOffset = (i - numStart);

                    for (; numStart < i; numStart++)
                    {
                        result += s[numStart];
                        commaOffset--;
                        if (commaOffset > 0 && (commaOffset % 3 == 0))
                        {
                            result += ',';
                        }
                    }

                    i--;    // already advanced i
                }
                else
                {
                    result += s[i];
                }
            }

            return result;
        }

        private static string ConvertInsideBrackets(string s)
        {
            string result = "";
            int inside = 0;

            for (int i = 0; i < s.Length; i++)
            {
                char c = s[i];

                if (c == '[' && inside > 0)
                {
                    c = '(';
                }

                if (s[i] == '[')
                {
                    inside++;              // pre-conversion
                }

                if (s[i] == ']')
                {
                    inside--;
                }

                if (c == ']' && inside > 0)
                {
                    c = ')';
                }

                result += c;
            }

            return result;
        }

        private static int FindNestedBracket(string str, int pos, char c)
        {
            char cend = ')';
            if (c == '[')
            {
                cend = ']';
            }

            if (c == '{')
            {
                cend = '}';
            }

            for (int count = 0; pos < str.Length; pos++)
            {
                if (str[pos] == c)
                {
                    count++;
                }
                else if (str[pos] == cend)
                {
                    count--;
                    if (count == 0)
                    {
                        return pos;
                    }
                    else if (count < 0)
                    {
                        return -1;
                    }
                }
            }

            return -1;
        }

        private static string SimplifyPercentages(Match m2)
        {
            string variableName = m2.Groups["VariableName"].ToString();
            double percentage = 1;
            bool hasOpeningParen = m2.Groups["OpeningParen"].Success;
            bool hasClosingParen = m2.Groups["ClosingParen"].Success;

            //if (variableName.ToLower().Contains("level"))
            if (variableName.ToLower().Contains("pl"))      // supposed to use $name not post-converted name
            {
                return m2.Groups[0].ToString();
            }

            if (variableName == "pctH")
            {
                return m2.Groups[0].ToString();
            }

            CaptureCollection cc = m2.Groups["Percentage"].Captures;
            for (int i = 0; i < cc.Count; ++i)
            {
                percentage *= double.Parse(cc[i].Value.Replace(",", ""));
            }

            if (hasOpeningParen && hasClosingParen)
            {
                hasOpeningParen = hasClosingParen = false;
            }

            percentage = Math.Round(percentage * 100, 2);

            StringBuilder ret = new StringBuilder();

            if (hasOpeningParen)
            {
                ret.Append(m2.Groups["OpeningParen"].Value);
            }


            // 135288 and 130735 seem weird without something in front... "xxxx or stamina" // but with, it seems really spammy... idk
            // only run the 'else' to always put something in front (also need to fix calc.cs to do the same)

            //if (percentage == 100)
            //    ret.Append("$" + variableName);
            //else
            ret.Append(String.Format("{0}% of ${1}", percentage, variableName));


            if (hasClosingParen)
            {
                ret.Append(m2.Groups["ClosingParen"].Value);
            }

            return ret.ToString();
        }

        private static string RemoveUnneededParenthesis(string input)
        {
            Stack<ParenthesisInformation> stack = new Stack<ParenthesisInformation>();
            bool isUseful = true;

            for (int i = 0; i < input.Length; ++i)
            {
                char c = input[i];

                if (c == '(' || c == '[')
                {
                    stack.Push(new ParenthesisInformation { StartPosition = i, Empty = !isUseful });
                    isUseful = false;
                    continue;
                }

                if (c == ')' || c == ']')
                {
                    if (stack.Count == 0)
                    {
                        continue; // Mismatched parenthesis
                    }

                    ParenthesisInformation info = stack.Pop();

                    if (info.Empty)
                    {
                        // Warning: Do not change the order of the next two lines.
                        input = input.Remove(i, 1);
                        input = input.Remove(info.StartPosition, 1);

                        i -= 2;
                    }

                    continue;
                }

                if (c != ' ' && stack.Count > 0)
                {
                    isUseful = true;
                }
                //stack.Peek().Empty = false;
            }

            return input;
        }

        private void LogDebug(string text, params object[] args)
        {
#if DEBUG
            if (args.Length > 0)
                text = String.Format(text, args);
            using (var w = System.IO.File.AppendText(@"c:\temp\sdp_debug.txt"))
                w.WriteLine(text);
#endif
        }
        #endregion

        #region Misc things
        private enum VariableParseEnum
        {
            Nothing,
            MathNumber,
            SpellID,
            Name,
            EffectID,
            Finished,
        };

        private class ParenthesisInformation
        {
            public int StartPosition;
            public bool Empty = true;
        }
        #endregion
    }
}
