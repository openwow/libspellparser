﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OpenWoW.LibSpellParser.Implementation
{
    internal static class Condition
    {
        private static Dictionary<string, int> _operatorsByPriority = new Dictionary<string, int>
        {
            { "|", 1 },
            { "&", 2 },
            { "=", 3 },
            { "!=", 3 },
            { ">", 4 },
            { "<", 4 },
        };

        private static readonly Regex _spellRegex = new Regex(@"[sap]([0-9]+)", RegexOptions.Compiled);
        private static readonly Regex _treeRegex = new Regex(@"c([0-9]+)", RegexOptions.Compiled);

        private static string replaceVariables(string input, HashSet<int> knownSpells, int activeTreeId)
        {
            input = _spellRegex.Replace(input, delegate (Match m)
            {
                int spellId = int.Parse(m.Groups[1].ToString());

                if (knownSpells.Contains(spellId))
                    return "1";
                else
                    return "0";
            });

            input = _treeRegex.Replace(input, delegate (Match m)
            {
                int treeId = int.Parse(m.Groups[1].ToString()) - 1; // c1 = tree 0, c3 = tree 2 ?

                    if (activeTreeId == treeId)
                    return "1";
                else
                    return "0";
            });

            return input;
        }

        public static bool IsConditionTrue(string condition, HashSet<int> knownSpells, int activeTreeId)
        {
            return IsConditionTrue(replaceVariables(condition, knownSpells, activeTreeId));
        }

        public static bool IsConditionTrue(string condition)
        {
            List<string> rpn;

            try
            {
                rpn = GetRpn(condition);
            }
            catch (Exception)
            {
                return false;
            }

            if (rpn.Count == 0)
                throw new Exception("Empty formula");

            Stack<int> intermediaryResults = new Stack<int>();
            foreach (string input in rpn)
            {
                if (!_operatorsByPriority.ContainsKey(input))
                {
                    intermediaryResults.Push(int.Parse(input));
                    continue;
                }

                string operation = input;
                int second = (intermediaryResults.Count == 0) ? 0 : intermediaryResults.Pop();
                int first = (intermediaryResults.Count == 0) ? 0 : intermediaryResults.Pop();

                int result;

                switch (operation)
                {
                    case "|":
                        result = (first != 0 || second != 0) ? 1 : 0;
                        break;
                    case "&":
                        result = (first != 0 && second != 0) ? 1 : 0;
                        break;
                    case "=":
                        result = (first == second) ? 1 : 0;
                        break;
                    case "!=":
                        result = (first != second) ? 1 : 0;
                        break;
                    case ">":
                        result = (first > second) ? 1 : 0;
                        break;
                    case "<":
                        result = (first < second) ? 1 : 0;
                        break;
                    default:
                        throw new Exception("Unknown Operator: " + operation);
                }

                intermediaryResults.Push(result);
            }

            if (intermediaryResults.Count != 1)
                throw new Exception("Invalid formula");

            return intermediaryResults.Peek() != 0 ? true : false;
        }

        public static List<string> GetRpn(string condition, HashSet<int> knownSpells, int activeTreeId)
        {
            return GetRpn(replaceVariables(condition, knownSpells, activeTreeId));
        }

        public static List<string> GetRpn(string formula)
        {
            List<string> output = new List<string>();
            Stack<string> stack = new Stack<string>();

            for (int position = 0; position < formula.Length; ++position)
            {
                char c = formula[position];

                if (c == ' ')
                    continue;

                // Is the token a number?
                int number;

                if (TryGetNumber(formula, ref position, out number))
                {
                    output.Add(number.ToString());
                    continue;
                }

                // Is the token an operator?
                string operatorName;

                if (TryGetOperator(formula, ref position, out operatorName))
                {
                    // while there is an operator token, o2, at the top of the stack, and o1's precedence is less than that of o2 pop o2 off the stack, onto the output queue;
                    while (true)
                    {
                        string o2 = stack.Count == 0 ? null : stack.Peek();

                        if (o2 == null)
                            break;

                        if (!_operatorsByPriority.ContainsKey(o2))
                            break;

                        if (_operatorsByPriority[operatorName] /* o1 */ > _operatorsByPriority[o2])
                            break;

                        output.Add(stack.Pop());
                    }

                    stack.Push(operatorName);
                    continue;
                }

                if (c == '(')
                {
                    stack.Push(c.ToString());
                    continue;
                }

                if (c == ')')
                {
                    while (true)
                    {
                        if (stack.Count == 0)
                            throw new Exception("Mismatched parenthesis");

                        if (stack.Peek() == "(")
                        {
                            stack.Pop();
                            break;
                        }

                        output.Add(stack.Pop());
                    }

                    continue;
                }

                throw new Exception("Unknown or invalid token in formula");
            }

            while (stack.Count > 0)
            {
                if (stack.Peek() == "(")
                    throw new Exception("Mismatched parenthesis");

                output.Add(stack.Pop());
            }

            return output;
        }

        /// <summary>
        /// Attempts to read an operator. Returns true if found, false if none.
        /// </summary>
        static bool TryGetOperator(string formula, ref int position, out string operatorNameResult)
        {
            foreach (var keyValue in _operatorsByPriority)
            {
                string operatorName = keyValue.Key;

                if (formula.Substring(position, Math.Min(operatorName.Length, formula.Length - position)) == operatorName)
                {
                    operatorNameResult = operatorName;
                    position += (operatorName.Length - 1);
                    return true;
                }
            }

            operatorNameResult = null;
            return false;
        }

        /// <summary>
        /// Attempts to sub sequentially read 1 or more characters containing a number.
        /// If it finds none, it returns false
        /// Otherwise it returns true + the number
        /// </summary>
        static bool TryGetNumber(string formula, ref int position, out int number)
        {
            StringBuilder ret = new StringBuilder();

            int i;
            for (i = position; i < formula.Length; ++i)
            {
                char c = formula[i];

                if (!(c >= '0' && c <= '9') && c != '!')
                    break;

                if (i != position && c == '!')
                    break;

                ret.Append(c);
            }

            if (ret.Length == 0 || ret.ToString() == "!")
            {
                number = 0;
                return false;
            }

            position = i - 1;

            if (ret[0] == '!')
            {
                ret.Remove(0, 1);
                number = int.Parse(ret.ToString()) == 0 ? 1 : 0;
            }
            else
                number = int.Parse(ret.ToString());

            return true;
        }

    }
}
