﻿using System;
using System.Collections.Generic;

namespace OpenWoW.LibSpellParser.Implementation
{
    public class MathInfo
    {
        private List<MathOperation> operations = new List<MathOperation>();
        private List<double> values = new List<double>();

        public void Parse(string operationStr, string valueStr)
        {
            MathOperation operation = MathOperation.None;

            switch (operationStr)
            {
                case "+":
                    operation = MathOperation.Plus;
                    break;
                case "-":
                    operation = MathOperation.Minus;
                    break;
                case "*":
                    operation = MathOperation.Multiply;
                    break;
                case "/":
                    operation = MathOperation.Divide;
                    break;
                default:
                    throw new InvalidOperationException(String.Format("Unknown operation: {0}", operation));
            }

            double value = double.Parse(valueStr);

            if (operation != MathOperation.None)
                Add(operation, value);
        }

        public void Add(MathOperation operation, double value)
        {
            operations.Add(operation);
            values.Add(value);
        }

        public void Apply(ref double number, bool doAbs)
        {
            for (int i = 0; i < operations.Count; ++i)
            {
                MathOperation operation = operations[i];
                double value = values[i];

                switch (operation)
                {
                    case MathOperation.Minus:
                        number -= value;
                        break;
                    case MathOperation.Plus:
                        number += value;
                        break;
                    case MathOperation.Divide:
                        number /= value;
                        break;
                    case MathOperation.Multiply:
                        number *= value;
                        break;
                    default:
                        break;
                }
            }

            if (doAbs)
                number = Math.Abs(number);
        }

        public void Clear()
        {
            operations.Clear();
            values.Clear();
        }
    }

    public enum MathOperation
    {
        None,

        Minus,
        Plus,
        Divide,
        Multiply
    }
}
