﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace OpenWoW.LibSpellParser.Implementation
{
    public class Calc
    {
        private static readonly Regex numbersWithoutThousandsSeparators = new Regex(@"(\.)?([0-9]{4,})", RegexOptions.Compiled);

        /// <summary>
        /// Evaluates a formula such as "5 + 5"; if it's not possible it tries to semplify it as much as it can.
        /// </summary>
        /// <param name="perfectSuccess">Set to true if the formula has been COMPLETELY parsed down to a number</param>
        public string Evaluate(string formula, bool recursiveCall, bool convertToPcts, out bool perfectSuccess, int overridePrecision)
        {
            bool _;

            return Evaluate(formula, recursiveCall, convertToPcts, out perfectSuccess, out _, overridePrecision);
        }

        public string Evaluate(string formula, bool recursiveCall, bool convertToPcts, out bool perfectSuccess, out bool nothingToDo, int overridePrecision)
        {
            formula = ConvertDollarParens(formula);

            if (formula.StartsWith("${")) formula = formula.Substring(2);

            formula = formula.Replace("\r", "");
            formula = formula.Replace("[ ", "(");
            formula = formula.Replace(" ]", ")");
            formula = formula.Replace("[", "(");
            formula = formula.Replace("]", ")");

            Object result = Expression.FromExpressionString(formula, recursiveCall, -1);        // -1 for precision --> use max precision until the end

            if (overridePrecision == -1) overridePrecision = 0;
            if (result is Expression) result = (result as Expression).ToString(recursiveCall, overridePrecision);

            bool addBrackets = false;
            if (result is String)
            {
                string s = result.ToString();
                if (s[0] != '[' || findNestedBracket(s, 0, '[') == s.Length - 1)    // if formula is [ ... ] then fine, but if it has stuff added on the end, dont add more brackets
                {
                    addBrackets = true;
                }
            }

            string finalResult = PostProcessComputedString(result as String, recursiveCall, addBrackets, overridePrecision);

            if (finalResult.StartsWith("(") && finalResult.EndsWith(")"))
            {
                finalResult = "[ " + finalResult.Substring(1, finalResult.Length - 2) + " ]";
            }
            finalResult = finalResult.Replace("(", "[");
            finalResult = finalResult.Replace(")", "]");

            finalResult = finalResult.Replace(" or 0]", "]");
            //finalResult = finalResult.Replace(" or 0 ", " ");
            finalResult = finalResult.Replace("[0 or ", "[");
            finalResult = finalResult.Replace(" 0 or ", " ");

            perfectSuccess = true;
            nothingToDo = false;

            return finalResult;
        }

        private string ConvertDollarParens(string s)
        {
            while (true)
            {
                int p = s.IndexOf("${");
                if (p == -1) break;

                int p2 = findNestedBracket(s, p + 1, '{');
                if (p2 == -1) break; // unbalanced

                s = s.Substring(0, p) + "(" + s.Substring(p + 2, p2 - (p + 2)) + ")" + s.Substring(p2 + 1); ;
            }
            return s;
        }


        private static int findNestedBracket(string str, int pos, char c)
        {
            char cend = ')';
            if (c == '[') cend = ']';
            if (c == '{') cend = '}';

            for (int count = 0; pos < str.Length; pos++)
            {
                if (str[pos] == c) count++;
                else if (str[pos] == cend)
                {
                    count--;
                    if (count == 0) return pos;
                    else if (count < 0) return -1;
                }
            }

            return -1;
        }


        private string PostProcessComputedString(string s, bool recursiveCall, bool addBrackets, int overridePrecision)
        {
            bool maybeNegative = s.StartsWith("-");

            if ((maybeNegative && ContainsMathOperators(s.Substring(1))) ||
               (!maybeNegative && ContainsMathOperators(s)))
            {

                // add spaces between tokens, but dont break up a negative sign from its number

                string result = "";
                bool firstChar = true;

                for (int i = 0; i < s.Length; i++)
                {
                    char cprev = (i - 1 >= 0) ? s[i - 1] : (char)0;
                    char c = s[i];
                    char cnext = (i + 1 < s.Length) ? s[i + 1] : (char)0;
                    char cnextnext = (i + 2 < s.Length) ? s[i + 2] : (char)0;
                    char cnextnextnext = (i + 3 < s.Length) ? s[i + 3] : (char)0;

                    if (cprev == ' ' && c == 'o' && cnext == 'r' && cnextnext == ' ')
                    {
                        i += 3 - 1;
                        firstChar = true;
                        result += "or ";
                        continue;
                    }

                    if (cprev == ' ' && c == 'a' && cnext == 'n' && cnextnext == 'd' && cnextnextnext == ' ')
                    {
                        i += 4 - 1;
                        firstChar = true;
                        result += "and ";
                        continue;
                    }


                    if (c == ' ' && (result.Length == 0 || result[result.Length - 1] == ' ')) continue;

                    if ("+-*/".Contains(c) && !(c == '-' && firstChar))
                    {
                        if (!firstChar)
                        {
                            if (s.Substring(i).ToLower().StartsWith("-handed"))
                            {
                                result += c;
                            }
                            else
                            {
                                result += " " + c + " ";
                            }
                        }
                        else
                        {
                            result += c;
                        }

                        firstChar = true;
                        continue;
                    }

                    result += c;
                    firstChar = false;
                    if (c == '(' || c == '[' || c == ',') firstChar = true;
                    //if (c == ',') result += " ";
                }
                if (addBrackets) result = "[ " + result + " ]";

                return result;
            }
            else
            {
                double val;
                if (double.TryParse(s, out val))
                {
                    val = Math.Abs(val);
                    if (recursiveCall)
                    {
                        return val.ToString(); // val.ToTooltipNumber(Math.Round(val) != val ? 1 : 0); ;
                    }
                    else
                    {
                        return (overridePrecision == -1) ? val.ToTooltipNumberAuto(!recursiveCall) : val.ToTooltipNumber(overridePrecision); // val.ToTooltipNumber(Math.Round(val) != val ? 1 : 0); ;
                    }
                }

                return s;
            }
        }



        bool ContainsMathOperators(string s)
        {
            return s.Contains('*') || s.Contains('+') || s.Contains('-') || s.Contains('/');
        }





        public class Term
        {
            public double coefficient;
            public Dictionary<String, int> units;   // <unit name, power> -- for 1/Unit ==> <Unit,-1> -- note: may want to change to <String,double> if we want square roots or something

            public Term()
            {
                units = new Dictionary<String, int>();
            }

            public Term(double val)
            {
                coefficient = val;
                units = new Dictionary<String, int>();
            }

            public Term(Term src)
            {       // deep clone
                coefficient = src.coefficient;

                units = new Dictionary<String, int>();
                foreach (KeyValuePair<String, int> kvp in src.units)
                {
                    units.Add(kvp.Key, kvp.Value);
                }
            }

            public bool HasLikeUnits(Term b)
            {

                if (units.Count != b.units.Count)
                {
                    return false;
                }

                foreach (KeyValuePair<String, int> kvp in b.units)
                {
                    if (!units.ContainsKey(kvp.Key)) return false;  // unit MUST exist
                    if (units[kvp.Key] != kvp.Value) return false;  // unit must have same exponent
                }

                return true;    // if count is same and all units/exponents match up, then it's a like-term
            }


            public static Term MulDiv(Term a, Term b, bool divide)
            {
                Term result = new Term(a);

                if (!divide)
                {
                    result.coefficient *= b.coefficient;
                }
                else
                {
                    result.coefficient /= b.coefficient;
                }


                foreach (KeyValuePair<String, int> unit in b.units)
                {
                    if (result.units.ContainsKey(unit.Key))
                    {
                        result.units[unit.Key] += divide ? (-unit.Value) : unit.Value;
                    }
                    else
                    {
                        result.units.Add(unit.Key, divide ? (-unit.Value) : unit.Value);
                    }
                }

                return result;
            }


            public static KeyValuePair<string, Dictionary<string, int>>[] unitReplaces = new KeyValuePair<string, Dictionary<string, int>>[]
            {
            new KeyValuePair<string, Dictionary<string, int>> ( "$curse_mh_dps_low", new Dictionary<string,int> { { "$mwb", 1 }, { "$mws", -1} } ),
            new KeyValuePair<string, Dictionary<string, int>> ( "$curse_mh_dps_high", new Dictionary<string,int> { { "$MWB", 1 }, { "$mws", -1} } ),
            new KeyValuePair<string, Dictionary<string, int>> ( "$curse_mh_dps_low", new Dictionary<string,int> { { "$mwb", 1 }, { "$MWS", -1} } ),
            new KeyValuePair<string, Dictionary<string, int>> ( "$curse_mh_dps_high", new Dictionary<string,int> { { "$MWB", 1 }, { "$MWS", -1} } ),

            new KeyValuePair<string, Dictionary<string, int>> ( "$curse_oh_dps_low", new Dictionary<string,int> { { "$owb", 1 }, { "$ows", -1} } ),
            new KeyValuePair<string, Dictionary<string, int>> ( "$curse_oh_dps_high", new Dictionary<string,int> { { "$OWB", 1 }, { "$ows", -1} } ),
            new KeyValuePair<string, Dictionary<string, int>> ( "$curse_oh_dps_low", new Dictionary<string,int> { { "$owb", 1 }, { "$OWS", -1} } ),
            new KeyValuePair<string, Dictionary<string, int>> ( "$curse_oh_dps_high", new Dictionary<string,int> { { "$OWB", 1 }, { "$OWS", -1} } ),
            };


            public static Term ConsolidateUnits(Term oldt)
            {
                Term t = new Term(oldt);

                while (true)
                {
                    bool newStuff = false;

                    foreach (var combo in unitReplaces)     // for each row, try to match set of units
                    {
                        bool isMatch = true;

                        foreach (var item in combo.Value)
                        {
                            if (!t.units.ContainsKey(item.Key))     // must contain unit name
                            {
                                isMatch = false;
                                break;
                            }

                            if (item.Value > 0 && t.units[item.Key] < item.Value)   // if we desire POSITIVE exponent, must contain at least that much exponent
                            {
                                isMatch = false;
                                break;
                            }

                            if (item.Value < 0 && t.units[item.Key] > item.Value)   // if we desire NEGATIVE exponent, must contain at least less than that much exponent
                            {
                                isMatch = false;
                                break;
                            }
                        }

                        if (isMatch)
                        {
                            newStuff = true;

                            foreach (var item in combo.Value)
                            {
                                t.units[item.Key] -= item.Value;    // remove units; 1 minus 1, -1 minus -1, etc
                            }


                            if (t.units.ContainsKey(combo.Key))     // add new combined unit
                            {
                                t.units[combo.Key]++;
                            }
                            else
                            {
                                t.units.Add(combo.Key, 1);
                            }

                        }

                    }


                    if (!newStuff) break;
                }

                return t;
            }

            public string ToString(bool firstTerm, int overridePrecision)
            {
                return this.ToString(firstTerm, false, overridePrecision);
            }

            // if index is 0, put normal number
            // if index >= 1, add a "+" operator in front if positive

            public string ToString(bool firstTerm, bool recursiveCall, int overridePrecision)
            {
                string s = "";

                if (Math.Abs(coefficient) <= 0.000000001)
                {   // rounding errors ==> just remove the term entirely if it's close to zero
                    return "";
                }


                Term tempTerm = ConsolidateUnits(this);

                foreach (KeyValuePair<String, int> unit in tempTerm.units)
                {
                    if (unit.Value == 0)
                    {
                        continue;   // just exclude this unit -- it's now equal to x^0 = 1
                    }

                    for (int i = 0; i < unit.Value; i++)
                    {
                        s += "*" + unit.Key;        // x^k
                    }

                    for (int i = 0; i > unit.Value; i--)
                    {
                        s += "/" + unit.Key;        // x^(-k)
                    }
                }

                string prefix = "";



                if (!firstTerm && coefficient > 0)
                {
                    prefix = "+";
                }

                // cases: 5
                //        -5
                //        +5
                //        -5


                // would it make more sense probably to say 100% of SP or 100% of Stamina than just "Stamina" ?, esp. e.g., on 135288
                // but we cant leave the 1* on for everything -- only use it for certain recognized units
                // for now this just removes it.... [check revision history for old code feature]

                if (s != "" && s[0] == '*' && Math.Abs(Math.Abs(coefficient) - 1) < 0.000000001)
                {   // uses units AND coefficient is close to +/- 1, just return the units
                    if (coefficient < 0)
                    {
                        prefix = "-";
                    }
                    else
                    {
                    }
                    return prefix + s.Substring(1);     // cut off the * front of s
                }


                if (!recursiveCall && units.Count > 1 && units.ContainsKey("$pctH"))      // if pctH ALSO WITH something else, outer parser wont expand it to a percent
                {
                    if (overridePrecision == -1)
                        return prefix + (100.0 * coefficient).ToTooltipNumberAuto(!recursiveCall) + "% of " + s.Substring(1); // get rid of the prefix "*" too
                    else
                        return prefix + (100.0 * coefficient).ToTooltipNumber(overridePrecision) + "% of " + s.Substring(1); // get rid of the prefix "*" too
                }
                else
                {
                    if (recursiveCall)
                    {
                        return prefix + coefficient.ToString("F12") + s;     // need full precision
                    }
                    else if (s != "" && s.ToLower() != "*$pcth" && s.ToLower() != "*$pl")
                    {
                        if (overridePrecision == -1)
                            return prefix + coefficient.ToTooltipNumberAuto(!recursiveCall, 4) + s;     // will probably be converted to a percent, so this should be up to 2 decimal points after multiplying by 100.0
                        else
                            return prefix + coefficient.ToTooltipNumber(3 + overridePrecision) + s;     // will probably be converted to a percent, so this should be up to 2 decimal points after multiplying by 100.0
                    }
                    else
                    {
                        if (overridePrecision == -1 || s.ToLower() == "*$pl")
                            return prefix + coefficient.ToTooltipNumberAuto(!recursiveCall) + s;
                        else
                        {
                            string tmp = coefficient.ToTooltipNumber(overridePrecision);
                            if (tmp == "0") return "";
                            return prefix + tmp + s;
                        }
                    }
                }
            }
        };


        public class Expression
        {
            public List<Term> terms;
            public List<Term> denominator = null;   // if null, only has numerator; otherwise, it is a fraction

            public Expression next = null;          // sibling Expression for something like "A or B" -- B would be A's .next, and B's next would be null
            public string nextOp;                   // the operation used to join this and next, e.g. "or"


            public Expression()
            {
                terms = new List<Term>();
            }

            public Expression(List<Term> numerator)
            {
                terms = new List<Term>();

                if (numerator != null)
                {
                    for (int i = 0; i < numerator.Count; i++)
                    {
                        terms.Add(new Term(numerator[i]));
                    }
                }
                else
                {
                    terms.Add(new Term(1.0));
                }
            }

            public Expression(string s)
            {   // note: ONLY accepts simple token!! e.g., 'a' and not '2*a' or anything like that
                terms = new List<Term>();
                Term term = new Term();

                double coef = 1.0;
                string sNum = s;

                if (s.EndsWith("%"))
                {
                    sNum = s.Substring(0, s.Length - 1);
                    coef = 0.01;
                }
                else if (s.EndsWith(" sec"))
                {
                    sNum = s.Substring(0, s.Length - 4);
                }
                else if (s.EndsWith("sec"))
                {
                    sNum = s.Substring(0, s.Length - 3);
                }

                double number;
                if (double.TryParse(sNum, out number))
                {
                    term.coefficient = number * coef;
                }
                else
                {
                    term.coefficient = 1;
                    term.units.Add(s, 1);
                }


                terms.Add(term);
            }


            // the key is always stripped
            // the bool value specifies whether it is case insensitive (true) or not (false)

            private static Dictionary<string, bool> functionNameStrip = new Dictionary<string, bool>() {
            { "$floor", true },
        };


            // returns a string representation of "function(param1, param2, etc)"

            private static Object functionAsString(string name, List<Object> parameters)
            {
                string result = name;

                foreach (var nameStrip in functionNameStrip)
                {
                    if (name.Equals(nameStrip.Key, nameStrip.Value ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture))
                    {
                        result = "";
                        break;
                    }

                }


                if (result == "" && parameters.Count == 1)
                {
                    return parameters[0];   // if this is just going to be a single object return string, just return the original object!
                }

                //Console.WriteLine("call {0} (", name);

                result += "(";
                for (int j = 0; j < parameters.Count; j++)
                {
                    //Console.WriteLine("  {0}", c[j].ToString());
                    result += parameters[j].ToString();
                    if (j != parameters.Count - 1) result += ", ";
                }
                //Console.WriteLine(");");
                result += ")";

                return result;
            }


            private static Object dispatchFunction(string name, List<Object> parameters)
            {

                /* simple example functions:

                    if (name == "$PL")
                    {
                        return new Expression("90");
                    }
                    else if (name == "$sum")
                    {
                        Expression e = new Expression("0");
                        for (int i = 0; i < parameters.Count; i++)
                        {
                            e.AddSub(parameters[i] as Expression, false);
                        }
                        return e;
                    }

                */

                if (name.StartsWith("1*")) name = name.Substring(2); // 1*$max for some reason becomes a single token

                // $cond(a,b,c) is like a ? b : c  ==>  just write back "(b or c)"

                if (name.Equals("$cond", StringComparison.InvariantCultureIgnoreCase) && parameters.Count == 3)
                {
                    if (parameters[0].ToString() == "1") return parameters[1];
                    if (parameters[0].ToString() == "0") return parameters[2];

                    if (parameters[1] is Expression && parameters[2] is Expression)
                    {
                        (parameters[1] as Expression).AndOr(parameters[2] as Expression, true);
                        return parameters[1];
                    }
                    else
                    {
                        return "(" + parameters[1].ToString() + " or " + parameters[2].ToString() + ")";
                    }
                }
                else if ((name.Equals("$max", StringComparison.InvariantCultureIgnoreCase) || name.Equals("$min", StringComparison.InvariantCultureIgnoreCase)) && parameters.Count == 2)
                {
                    // If the parameters are something that looks like doubles, just do the min or max already
                    var a = parameters[0].ToString();
                    var b = parameters[1].ToString();
                    double da, db;
                    if (double.TryParse(a, out da) && double.TryParse(b, out db))
                    {
                        if (name.Equals("$max", StringComparison.InvariantCultureIgnoreCase))
                        {
                            return Math.Max(da, db).ToString();
                        }
                        else
                        {
                            return Math.Min(da, db).ToString();
                        }
                    }

                    //if (parameters[0].ToString() == "0") return parameters[1];
                    //if (parameters[1].ToString() == "0") return parameters[0];

                    if (parameters[0] is Expression && parameters[1] is Expression)
                    {
                        (parameters[0] as Expression).AndOr(parameters[1] as Expression, true);
                        return parameters[0];
                    }
                    else
                    {
                        //return "(" + parameters[0].ToString() + " or " + parameters[1].ToString() + ")";  // this is commented out -- fallback to default max(a,b) equation if already string
                    }
                }
                else if (name.Equals("$clamp", StringComparison.InvariantCultureIgnoreCase) && parameters.Count == 3)
                {
                    // $clamp(-25[value],-80[min],-25[max]) = Min(-25, Max(-80, -25))
                    try
                    {
                        return (Math.Min(double.Parse(parameters[2].ToString()), Math.Max(double.Parse(parameters[1].ToString()), double.Parse(parameters[0].ToString())))).ToString();
                    }
                    catch (FormatException)
                    {
                        return string.Format("[ {0} ] [Min {1}, Max {2}]", parameters[0], parameters[1], parameters[2]);
                    }
                }
                else if (name.Equals("$abs", StringComparison.InvariantCultureIgnoreCase) && parameters.Count == 1)
                {
                    return (Math.Abs(double.Parse(parameters[0].ToString()))).ToString();
                }


                // default: just return as a string since we can't parse it :(

                return functionAsString(name, parameters);
            }



            // returns e.g. "a+b" but in such a way that retains OoO

            public static Object EqStr(Object _a, string op, Object _b)
            {
                string a = _a.ToString();
                string b = _b.ToString();

                if (op == "*" && a == "1") return b;
                if (op == "*" && b == "1") return a;
                if (op == "/" && b == "1") return a;

                if (op == "*" && (a == "0" || b == "0")) return "0";
                if (op == "/" && (a == "0" || b == "0")) return "0";

                double da, db;
                if (double.TryParse(a, out da) && double.TryParse(b, out db))
                {
                    if (op == "*")
                    {
                        return (da * db).ToString();
                    }
                    else if (op == "/" && da != 0 && db != 0)
                    {
                        return (da / db).ToString();
                    }
                    else if (op == "+")
                    {
                        return (da + db).ToString();
                    }
                    else if (op == "-")
                    {
                        return (da - db).ToString();
                    }
                }

                if (op == "*" || op == "/" || op == "-")
                {
                    a = "(" + a + ")";
                    b = "(" + b + ")";
                }

                return a + op + b;
            }


            public void NegateSimple()
            {    // todo: this is not thoroughly tested
                for (int i = 0; i < terms.Count; i++)
                {
                    terms[i].coefficient = -terms[i].coefficient;
                }
                if (next != null) next.NegateSimple();
            }



            // converts a string to an Expression (or simplified string, if not everything could be evaluated) -- check return type

            public static Object FromExpressionString(string first, bool recursiveCall, int overridePrecision)
            {
                Stack<Object> stack = new Stack<Object>();


                // if this expression contains some unparseable symbols then just return the String version [old code removed; see revision history]




                if (first.EndsWith(" sec PvP"))        // special case implicit math =/  dont modify this!
                {
                    return "(" + first + ")";
                }

                first = first.Replace(" sec", "");
                List<string> rpn1 = Rpn(first);

                if (rpn1 == null) return first;



                for (int i = 0; i < rpn1.Count; i++)
                {
                    if (!IsMathOperator(rpn1[i]) && rpn1[i] != "," && rpn1[i] != "or" && rpn1[i] != "#callFunction")
                    {
                        if (rpn1[i] == "#paramList") stack.Push(rpn1[i]);
                        else stack.Push(new Expression(rpn1[i]));
                        continue;
                    }


                    Object b = stack.Pop();
                    Object a = stack.Pop();

                    if (IsMathOperator(rpn1[i]))     // dont do this if we're calling a function -- only do if it it's some kind of binary operator (e.g. a OP b)
                    {
                        if (b is Expression && (b as Expression).next != null)
                        {
                            if (a is Expression && (a as Expression).next != null)
                            {
                                // if both expressions have an and/or expansion, then we can't handle them currently
                                // only A can have these expansions

                                a = (a as Expression).ToString(recursiveCall, overridePrecision);
                                b = (b as Expression).ToString(recursiveCall, overridePrecision);
                            }
                            else
                            {
                                if (rpn1[i] == "+" || rpn1[i] == "*")
                                {
                                    Object c = a;
                                    a = b;
                                    b = c;
                                }
                                else if (rpn1[i] == "-")
                                {
                                    Object c = a;
                                    a = b;
                                    b = c;

                                    // a - b  (where b is a chain with .next) ==> -b + a  (but note that a and b are now swapped for the below code)

                                    (a as Expression).NegateSimple();
                                    rpn1[i] = "+";
                                }
                                else
                                {
                                    // same as above, we can't handle these other cases as their order matters
                                    // for subtraction we could potentially change the sign on each term or something like that, but for now, it just fixes a couple cases

                                    a = (a as Expression).ToString(recursiveCall, overridePrecision);
                                    b = (b as Expression).ToString(recursiveCall, overridePrecision);
                                }
                            }
                        }
                    }

                    if (rpn1[i] == "+")
                    {
                        if (a is String || b is String) a = EqStr(a, rpn1[i], b);
                        else (a as Expression).AddSub(b as Expression, false);
                    }
                    else if (rpn1[i] == "-")
                    {
                        if (a is String || b is String) a = EqStr(a, rpn1[i], b);
                        else (a as Expression).AddSub(b as Expression, true);
                    }
                    else if (rpn1[i] == "*")
                    {
                        if (a is String || b is String) a = EqStr(a, rpn1[i], b);
                        else (a as Expression).MulDiv(b as Expression, false);
                    }
                    else if (rpn1[i] == "/")
                    {
                        if (a is String || b is String) a = EqStr(a, rpn1[i], b);
                        else (a as Expression).MulDiv(b as Expression, true);
                    }
                    else if (rpn1[i] == "or")
                    {
                        if (a is String || b is String) a = EqStr(a, rpn1[i], b);
                        else (a as Expression).AndOr(b as Expression, true);
                    }
                    else if (rpn1[i] == ",")
                    {
                        if (a is List<Object>)
                        {
                            (a as List<Object>).Add(b);
                        }
                        else
                        {
                            List<Object> c = new List<Object>();
                            c.Add(a);
                            c.Add(b);
                            a = c;
                        }
                    }
                    else if (rpn1[i] == "#callFunction")
                    {
                        if (b is String && (b as String) == "#paramList")
                        {
                            b = new List<Object>();     // empty parameter list
                        }
                        else if (!(b is List<Object>))   // single parameter
                        {
                            List<Object> d = new List<Object>();
                            d.Add(b);
                            b = d;
                            a = stack.Pop();    // pop the function name over top #paramList
                        }
                        else
                        {
                            a = stack.Pop();    // pop the function name over top #paramList
                        }

                        a = dispatchFunction((a as Expression).ToString(), b as List<Object>);
                    }

                    stack.Push(a);
                }

                Object firstExp = stack.Pop();
                return firstExp;
            }



            public void AddTerm(Term term, bool subtract)
            {

                if (denominator != null)
                {

                    Expression numeratorSrc = new Expression(denominator);
                    Expression tmp = new Expression();
                    tmp.terms.Add(new Term(term));
                    numeratorSrc.MulDiv(tmp, false);            // denominator * term to have similar denominator ==> then add to fraction

                    Expression numerator = new Expression(terms);
                    numerator.AddSub(numeratorSrc, false);
                    terms = numerator.terms;                // denominator stays the same, numerator is the sum

                }
                else
                {

                    for (int j = 0; j < terms.Count; j++)
                    {
                        if (terms[j].HasLikeUnits(term))
                        {
                            terms[j].coefficient += subtract ? (-term.coefficient) : term.coefficient;
                            return;
                        }
                    }

                    Term newTerm = new Term(term);
                    if (subtract)
                    {
                        newTerm.coefficient = -newTerm.coefficient;
                    }

                    terms.Add(newTerm);
                }
            }



            public void AddSub(Expression src, bool subtract)
            {
                if (denominator != null && src.denominator != null)
                {

                    Expression n1 = new Expression(terms);
                    Expression d1 = new Expression(denominator);
                    Expression n2 = new Expression(src.terms);
                    Expression d2 = new Expression(src.denominator);

                    n1.MulDiv(d2, false);
                    n2.MulDiv(d1, false);
                    d1.MulDiv(d2, false);
                    n1.AddSub(n2, false);

                    terms = n1.terms;
                    denominator = d1.terms;

                }
                else
                {

                    for (int i = 0; i < src.terms.Count; i++)
                    {
                        AddTerm(src.terms[i], subtract);
                    }

                }

                if (next != null)
                {
                    next.AddSub(src, subtract);
                }
            }




            public void MulDiv(Expression src, bool divide)
            {
                if (denominator != null || src.denominator != null)
                {

                    Expression n1 = new Expression(terms);
                    Expression d1 = new Expression(denominator);
                    Expression n2 = new Expression(src.terms);
                    Expression d2 = new Expression(src.denominator);

                    if (!divide)
                    {
                        Expression tmp1 = new Expression(n1.terms);
                        tmp1.AddSub(d2, true);
                        if (tmp1.ToString() == "0")
                        {
                            n1 = new Expression("1");
                            d2 = new Expression("1");
                        }

                        Expression tmp2 = new Expression(n2.terms);
                        tmp2.AddSub(d1, true);
                        if (tmp2.ToString() == "0")
                        {
                            n2 = new Expression("1");
                            d1 = new Expression("1");
                        }


                        n1.MulDiv(n2, false);
                        d1.MulDiv(d2, false);

                    }
                    else
                    {

                        Expression tmp1 = new Expression(n1.terms);
                        tmp1.AddSub(n2, true);
                        if (tmp1.ToString() == "0")
                        {
                            n1 = new Expression("1");
                            n2 = new Expression("1");
                        }

                        Expression tmp2 = new Expression(d2.terms);
                        tmp2.AddSub(d1, true);
                        if (tmp2.ToString() == "0")
                        {
                            d2 = new Expression("1");
                            d1 = new Expression("1");
                        }


                        n1.MulDiv(d2, false);
                        d1.MulDiv(n2, false);
                    }

                    terms = n1.terms;
                    denominator = d1.terms;

                }
                else
                {

                    if (divide && src.terms.Count > 1)
                    {     // note that the NxN multiply expansion works for divide when src Count is 1, so this is correct (using the else block)

                        Expression d1 = new Expression(src.terms);      // this optimization is simple/stupid and we should move it to normalization instead
                        Expression tmp1 = new Expression(terms);
                        tmp1.AddSub(d1, true);
                        if (tmp1.ToString() == "0")             // if they're the same, return "1"
                        {
                            Expression n1 = new Expression("1");
                            terms = n1.terms;
                        }
                        else
                        {
                            denominator = d1.terms; // easy now, but we'll have to potentially normalize later...
                        }

                    }
                    else
                    {

                        // every source term * every dest term ==> accumulate inside a new Expression

                        Expression result = new Expression();

                        for (int j = 0; j < terms.Count; j++)
                        {
                            for (int i = 0; i < src.terms.Count; i++)
                            {
                                Term newTerm = Term.MulDiv(terms[j], src.terms[i], divide);
                                result.AddTerm(newTerm, false);
                            }
                        }

                        terms = result.terms;   // overwrite current terms with resulting terms
                    }
                }

                if (next != null)
                {
                    next.MulDiv(src, divide);
                }
            }


            public void AndOr(Expression src, bool isOr)
            {
                Expression e = this as Expression;
                while (e.next != null) e = e.next;               // attach C to the end of B
                e.next = src as Expression;
                e.nextOp = isOr ? "or" : "and";
            }




            public override string ToString()
            {
                return this.ToString(false, -1);
            }


            public string ToString(bool recursiveCall, int overridePrecision)
            {
                string num = "";
                string den = "";
                string result = "";


                // todo: do we want to prioritize term ordering? e.g. AP always comes before SP?

                for (int i = 0; i < terms.Count; i++)
                {
                    num += terms[i].ToString(num == "", recursiveCall, overridePrecision);
                }

                if (num == "")
                {
                    num = "0";
                }


                result = "" + num;

                if (denominator != null)
                {
                    for (int i = 0; i < denominator.Count; i++)
                    {
                        den += denominator[i].ToString(den == "", recursiveCall, overridePrecision);
                    }

                    if (den == "")
                    {
                        den = "0";
                    }

                    if (den != "1")
                    {
                        result = "(" + num + ") / (" + den + ")";
                    }
                }

                if (next != null)
                {
                    string nextStr = next.ToString(recursiveCall, overridePrecision);

                    if (nextStr.StartsWith("(") && nextStr.EndsWith(")"))       // cut off surrounding ()'s -- we might not want this later if we want to preserve op chaining precedence, but they're all at the same OoO level for now
                    {
                        nextStr = nextStr.Substring(1, nextStr.Length - 2);     // e.g. this would change (a or (b or (c or d))) to (a or b or c or d)
                    }

                    result = "(" + result + " " + nextOp + " " + nextStr + ")";
                }

                return result;
            }


        };




        static string BalanceParenthesis(string s)
        {
            int count = 0;

            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == '(') count++;
                else if (s[i] == ')') count--;
            }

            for (; count < 0; count++) s = '(' + s;
            for (; count > 0; count--) s = s + ')';

            return s;
        }

        /// <summary>
        /// Converts the formula to RPN (Reverse Polish Notation).
        /// </summary>
        static List<string> Rpn(string formula)
        {
            formula = BalanceParenthesis(formula);
            Stack<string> stack = new Stack<string>();
            Stack<string> operatorStack = new Stack<string>();
            bool expectingOperator = false;
            bool maybeFunction = false;     // var(...)


            for (int position = 0; position < formula.Length; ++position)
            {
                char c = formula[position];
                char cnext = (char)0;
                char cnextnext = (char)0;
                if (position < formula.Length - 1) cnext = formula[position + 1];
                if (position < formula.Length - 2) cnextnext = formula[position + 2];

                if (c == ' ')
                    continue;

                if (c == '(')
                {
                    if (maybeFunction)  // IS a function
                    {
                        stack.Push("#paramList");    // special token indicating function call parameter list
                    }
                    operatorStack.Push(c.ToString());
                    if (maybeFunction)  // IS a function
                    {
                        operatorStack.Push("#callFunction");    // special token indicating function call
                    }
                    expectingOperator = false;
                    maybeFunction = false;
                    continue;
                }

                if (c == ')')
                {
                    maybeFunction = false;
                    while (operatorStack.Count != 0 && operatorStack.Peek() != "(")
                        stack.Push(operatorStack.Pop());

                    if (operatorStack.Count == 0)
                        return null; // Major error - Mismatched parenthesis - We cannot do anything about this.

                    operatorStack.Pop();
                    expectingOperator = true;
                    continue;
                }

                if (!expectingOperator)
                {
                    while (formula.StartsWith("--")) formula = formula.Substring(2);    // not expecting an operator -- so two minuses is starting with a positive number

                    maybeFunction = false;
                    // We expect a number or variable to be here.
                    string value = TryGetValue(formula, ref position);

                    if (value != null)
                    {
                        position -= 1; // It was already at the correct position, but it will be incremented next iteration.

                        if (value == "-")   // not expecting operator now, so this is probably a unary minus not handled elsewhere, e.g. -(complex equation)
                        {
                            value = "0";    // convert it to 0-(complex equation)
                            position--;
                        }

                        stack.Push(value);
                        expectingOperator = true;
                        maybeFunction = true;
                        continue;
                    }
                }

                if (c == ',')
                {
                    maybeFunction = false;
                    while (operatorStack.Count != 0 && (operatorStack.Peek() == "," || operatorStack.Peek() == "or" || IsMathOperator(operatorStack.Peek())))
                        stack.Push(operatorStack.Pop());

                    operatorStack.Push(c.ToString());
                    expectingOperator = false;
                    continue;
                }

                if (c == 'o' && cnext == 'r' && cnextnext == ' ')
                {
                    maybeFunction = false;
                    while (operatorStack.Count != 0 && (operatorStack.Peek() == "or" || IsMathOperator(operatorStack.Peek())))
                        stack.Push(operatorStack.Pop());

                    position++; // chomp 2 chars instead of 1

                    operatorStack.Push("or");
                    expectingOperator = false;
                    continue;
                }

                if (c == '+' || c == '-')
                {
                    maybeFunction = false;
                    while (operatorStack.Count != 0 && IsMathOperator(operatorStack.Peek()))
                        stack.Push(operatorStack.Pop());

                    operatorStack.Push(c.ToString());
                    expectingOperator = false;
                    continue;
                }

                if (c == '*' || c == '/')
                {
                    maybeFunction = false;
                    while (operatorStack.Count != 0 && (operatorStack.Peek() == "*" || operatorStack.Peek() == "/"))
                        stack.Push(operatorStack.Pop());

                    operatorStack.Push(c.ToString());
                    expectingOperator = false;
                    continue;
                }

                /* Very strange character in formula - Fatal error */
                return null;
            }

            while (operatorStack.Count != 0)
            {
                if (!(operatorStack.Peek() == "or" || IsMathOperator(operatorStack.Peek())))
                    return null; // Mismatched parenthesis

                stack.Push(operatorStack.Pop());
            }




            return stack.Reverse().ToList();
        }

        static private bool IsMathOperator(string s)
        {
            return s.Length == 1 && IsMathOperator(s[0]);
        }

        static private bool IsMathOperator(char c)
        {
            if (c == '*' || c == '/' || c == '+' || c == '-')
                return true;

            return false;
        }

        static readonly Regex numberRegex = new Regex(@"[0-9,]+", RegexOptions.Compiled);

        /// <summary>
        /// Attempts to subsequentially read 1 or more characters containing a number, the $ literal, or a letter, a %, or spaces.
        /// If it finds none, it returns null
        /// Otherwise it returns the read characters and increases the position
        /// </summary>
        static string TryGetValue(string formula, ref int position)
        {
            StringBuilder ret = new StringBuilder();

            int i;
            for (i = position; i < formula.Length; ++i)
            {
                char c = formula[i];
                char cprev = (char)0;
                char cnext = (char)0;
                char cnextnext = (char)0;
                if (i > 0) cprev = formula[i - 1];
                if (i < formula.Length - 1) cnext = formula[i + 1];
                if (i < formula.Length - 2) cnextnext = formula[i + 2];


                if (!(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z') && !(c >= '0' && c <= '9') && c != '.' && c != '%' && c != ' ' && c != '$' && c != '_' && c != '-')  // no comma in here now -- needed to support function lists
                    break;

                if (i != position && (c == '$' || c == '-'))
                    break;

                if (i != position && (cprev == ' ' && c == 'o' && cnext == 'r' && cnextnext == ' '))
                    break;


                if (c != ',')
                    ret.Append(c);
            }

            if (ret.Length == 0)
                return null;

            position = i;
            string finalret = ret.ToString().Trim();

            if (finalret == "$MWS") finalret = "$mws";  // these should be case insensitive
            if (finalret == "$OWS") finalret = "$ows";

            return finalret;
        }
    }
}
