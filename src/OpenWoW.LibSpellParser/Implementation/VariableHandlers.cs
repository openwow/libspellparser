﻿using OpenWoW.Common;
using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.LibSpellParser.Implementation
{
    internal class VariableHandlers
    {
        private enum EffectPointsType
        {
            MinPoints,
            MaxPoints,
            MinMaxPoints
        }

        internal delegate string VariableHandler(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options);

        internal static Dictionary<string, VariableHandler> variableHandlers = new Dictionary<string, VariableHandler>
        {
            { "a", HandleEffectRadius1 },
            { "A", HandleEffectRadius2 },
            { "b", HandlePointsPerCombo },
            { "c", HandleCost },
            { "d", HandleDuration },
            { "D", HandleDuration },
            { "e", HandleEffectMultipleValue },
            { "f", HandleEffectDamageMultiplier },
            { "F", HandleEffectDamageMultiplier },
            { "ga", HandleGarrisonAbilityEffectPoints },
            { "gd", HandleGarrisonAbilityEffectDuration },
            { "h", HandleProcChance },
            { "H", HandleProcChance },
            { "i", HandleMaxTargets },
            { "I", HandleMaxTargets },
            { "m", HandleEffectPointsMin },
            { "M", HandleEffectPointsMax },
            { "n", HandleProcCharges },
            { "N", HandleProcCharges },
            { "o", HandleEffectPointsSum },
            { "pl", HandlePlayerLevel },
            { "PL", HandlePlayerLevel },
            { "proccooldown", HandleProcCooldown },
            { "procrppm", HandleProcPerMinDummy },
            { "q", HandleEffectMisc1 },
            { "r", HandleRange },
            { "R", HandleRangeMax },
            { "s", HandleEffectPoints },
            { "S", HandleEffectPointsMax },
            { "sw", HandleNewWeaponDamage },
            { "t", HandleEffectTime },
            { "T", HandleEffectTime },
            { "u", HandleMaxApplications },
            { "U", HandleMaxApplications },
            { "v", HandleMaxTargetLevel },
            { "w", HandleEffectPoints },
            { "W", HandleEffectPoints },
            { "x", HandleEffectChainTargets },
        };

        internal static Dictionary<string, string> variableTextReplaces = new Dictionary<string, string>
        {
            { "AP", "Attack Power" },
            { "ap", "Attack Power" },
            { "OAP", "Off-hand Attack Power" },
            { "oap", "Off-hand Attack Power" },
            { "RAP", "Ranged Attack Power" },
            { "rap", "Ranged Attack Power" },
            { "SPH", "Holy Spell Power" },
            { "sph", "Holy Spell Power" },
            { "SP", "Spell Power" },
            { "sp", "Spell Power" },
            { "bh", "Healing" },
            { "SPA", "Arcane Spell Power" },
            { "spa", "Arcane Spell Power" },
            { "SPN", "Nature Spell Power" },
            { "spn", "Nature Spell Power" },
            { "SPS", "Shadow Spell Power" },
            { "sps", "Shadow Spell Power" },
            { "SPFI", "Fire Spell Power" },
            { "spfi", "Fire Spell Power" },
            { "SPFR", "Frost Spell Power" },
            { "spfr", "Frost Spell Power" },
            { "z", "&lt;Hearthstone Location&gt;" },
            { "mwb", "Mainhand Min Damage" },
            { "MWB", "Mainhand Max Damage" },
            { "owb", "Offhand Min Damage" },
            { "OWB", "Offhand Max Damage" },
            { "rwb", "Ranged Min Damage" },
            { "RWB", "Ranged Max Damage" },
            { "MWS", "Mainhand Weapon Speed" },
            { "mws", "Mainhand Weapon Speed" },
            { "max", "max" },
            { "min", "min" },
            { "cond", "cond" },
            { "COND", "cond" },
            { "eq", "equal" },
            { "GT", "greater_then" },
            { "gt", "greater_then" },
            { "LT", "less_then" },
            { "lt", "less_then" },
            { "GTE", "greater_than_or_equal" },
            { "gte", "greater_than_or_equal" },
            { "LTE", "less_than_or_equal" },
            { "lte", "less_than_or_equal" },
            { "HND", "HND" },
            { "mw", "Mainhand Damage" },
            { "MW", "Mainhand Damage" },
            { "PlayerName", "Caster Name" },
            { "AR", "Armor" },
            { "FLOOR", "floor" },
            { "CEIL", "ceil" },
            { "floor", "floor" },
            { "ceil", "ceil" },
            { "MHP", "Total Health" },
            { "pctH", "Percent Health" },
            { "STA", "Stamina" },
            { "STR", "Strength" },
            { "SPI", "Spirit" },
            { "SPR", "Spirit" },
            { "AGI", "Agility" },
            { "INT", "Intellect" },


            // Curse - Custom variables
            { "curse_mh_dps", "Mainhand DPS" },
            { "curse_oh_dps", "Offhand DPS" },

            { "curse_mh_dps_low", "Mainhand Min DPS" },
            { "curse_mh_dps_high", "Mainhand Max DPS" },
            { "curse_oh_dps_low", "Offhand Min DPS" },
            { "curse_oh_dps_high", "Offhand Max DPS" },

            { "curse_oh_half_dps", "50% of Offhand DPS" },
            { "curse_oh_half_basedamage", "50% of Offhand Base Damage" },
        };

        internal static string HandleCost(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            IParserSpellPower power = spell.Powers.FirstOrDefault();
            double value = power != null ? power.ManaCost : 0;

            math.Apply(ref value, true);

            return value.ToTooltipNumber();
        }

        internal static string HandleDuration(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double value = spell.Duration?.Duration ?? 0;

            math.Apply(ref value, false);

            if (insideMath)
                return (value / 1000).ToString();
            else
                return TimeUtilities.GetTooltipTime((int)value, "until cancelled");
        }

        internal static string HandleEffectChainTargets(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double result = parser.GetSpellEffect(spell, effectId).ChainTargets;
            math.Apply(ref result, true);
            return result.ToTooltipNumber();
        }

        internal static string HandleEffectDamageMultiplier(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double result = parser.GetSpellEffect(spell, effectId).ChainAmplitude;
            math.Apply(ref result, true);
            return result.ToString();
        }

        internal static string HandleEffectMisc1(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double result = parser.GetSpellEffect(spell, effectId).Misc1;
            math.Apply(ref result, true);
            return result.ToTooltipNumberAuto(!recursiveCall);
        }

        internal static string HandleEffectMultipleValue(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double result = parser.GetSpellEffect(spell, effectId).Amplitude;
            math.Apply(ref result, true);
            return result.ToTooltipNumberAuto(!recursiveCall);
        }

        #region Effect Points
        internal static string HandleEffectPoints(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            return HandleEffectPoints(parser, spell, effectId, insideMath, recursiveCall, EffectPointsType.MinMaxPoints, baseSpell.ExternalID, math, options);
        }

        internal static string HandleEffectPointsMin(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            return HandleEffectPoints(parser, spell, effectId, insideMath, recursiveCall, EffectPointsType.MinPoints, baseSpell.ExternalID, math, options);
        }

        internal static string HandleEffectPointsMax(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            return HandleEffectPoints(parser, spell, effectId, insideMath, recursiveCall, EffectPointsType.MaxPoints, baseSpell.ExternalID, math, options);
        }

        internal static string HandleEffectPointsSum(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            IParserSpellEffect effect = parser.GetSpellEffect(spell, effectId);

            double time = effect.AuraPeriod == 0 ? 5000 : effect.AuraPeriod;
            double duration = spell.Duration?.Duration ?? 0;

            if (duration > 0)
            {
                math.Add(MathOperation.Multiply, duration / time);
            }

            return HandleEffectPoints(parser, spell, effectId, insideMath, recursiveCall, EffectPointsType.MinMaxPoints, baseSpell.ExternalID, math, options);
        }

        /// <summary>
        /// Returns the textual representation of the effect points. Example: "10 to 25".
        /// </summary>
        private static string HandleEffectPoints(IBaseSpellParser parser, IParserSpell spell, int effectId, bool insideMath, bool recursiveCall, EffectPointsType pointsType, int baseSpellExternalID,
            MathInfo math, SpellParserOptions options)
        {
            double points = 0, minPoints = 0, maxPoints = 0;
            IParserSpellEffect effect = parser.GetSpellEffect(spell, effectId);

            // Legion Artifact spells need to look up values based on rank
            bool foundArtifact = false;
            if (effectId == 0 && options.ArtifactRank > 0)
            {
                points = minPoints = maxPoints = spell.GetArtifactEffectPoints(options.ArtifactRank.Value);
                foundArtifact = (points != 0);
            }

            if (!foundArtifact)
            {
                effect.GetEffectPoints(out points, out minPoints, out maxPoints, options.ItemLevel, !insideMath, options.ItemSlot ?? 0,
                    options.ItemRarity, options.PlayerLevel);

                if (spell.Flags9.HasFlag(SpellFlags9.IsMastery) && effect.BonusCoefficientFromSP != 0)
                {
                    points = minPoints = maxPoints = (8 * effect.BonusCoefficientFromSP);
                }
            }

            // WOW-895: handle passive spells with effects that apply modifiers to this effect's value. Yeah, that's a thing.
            int modPoints = effect.GetModifierPoints(options.SpecializationSpellIDs);
            points += modPoints;
            minPoints += modPoints;
            maxPoints += modPoints;

            var extraParts = new List<string>();

            // Some spells with SpellPowerScaling=1 actually really do want to use "100% of SP", do our best to detect them here. [blame simca]
            bool forceSpellPowerScaling = HardcodedData.SpellPowerSpecializations.Contains(options.SpecializationID) &&
                !spell.Flags9.HasFlag(SpellFlags9.IsMastery) &&
                effect.Amplitude == 0 &&
                (effect.BasePoints == 0 || effect.BasePoints == 1) &&
                effect.ChainAmplitude == 1 &&
                effect.RandomPoints == 0 &&
                effect.Misc1 == 0 &&
                effect.Misc2 == 0 &&
                effect.PointsPerLevel == 0 &&
                effect.BonusCoefficientFromSP == 1 &&
                effect.BonusCoefficientFromAP == 0 &&
                effect.IsDamageOrHealingEffect();

            if ((effect.BonusCoefficientFromSP > 0 && effect.BonusCoefficientFromSP != 1 && !spell.Flags9.HasFlag(SpellFlags9.IsMastery)) || forceSpellPowerScaling)
            {
                // Apply math to the scaling coefficient too
                double spellPowerScaling = effect.BonusCoefficientFromSP;
                math.Apply(ref spellPowerScaling, !insideMath);

                extraParts.Add(String.Format("{0}*$SP", spellPowerScaling));
            }

            if (effect.IsDamageOrHealingEffect())
            {
                if (effect.BonusCoefficientFromAP != 0)
                {
                    // Apply math to the scaling coefficient too
                    double attackPowerScaling = effect.BonusCoefficientFromAP;
                    math.Apply(ref attackPowerScaling, !insideMath);

                    //result += String.Format(" + {0}*$AP + {1}*EffectType", spell.AttackPowerScaling, (int)effect.Type);   // debug
                    extraParts.Add(String.Format("{0}*$AP", attackPowerScaling));
                }
            }


            switch (pointsType)
            {
                case EffectPointsType.MaxPoints:
                    points = maxPoints;
                    break;
                case EffectPointsType.MinPoints:
                    points = minPoints;
                    break;
                case EffectPointsType.MinMaxPoints:
                default:
                    break;
            }

            // WOW-754: spells look really goofy with "1 + thing" or "n + thing" where n is tick count, replace that with a 0 here and remove it later
            if (points == 1 && extraParts.Count > 0)
            {
                points = 0;
            }

            //math.Apply(ref minPoints, !insideMath);
            //math.Apply(ref maxPoints, !insideMath);
            math.Apply(ref points, !insideMath);

            //string result = Math.Round(points, 0).ToTooltipNumber();
            //string result = points.ToTooltipNumberAuto(!recursiveCall);


            if (extraParts.Count > 0)
            {
                // We don't want to put "0 + " at the start of the text
                if (points == 0)
                {
                    return String.Format("[ {0} ]", String.Join(" + ", extraParts));
                }
                else
                {
                    return String.Format("[ {0} + {1} ]", points.ToTooltipNumberAuto(!recursiveCall), String.Join(" + ", extraParts));
                }
            }
            else
            {
                return points.ToTooltipNumberAuto(!recursiveCall);
            }
        }
        #endregion

        internal static string HandleEffectRadius1(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            return HandleEffectRadius(parser.GetSpellEffect(spell, effectId).Radius1, math);
        }

        internal static string HandleEffectRadius2(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            var effect = parser.GetSpellEffect(spell, effectId);
            return HandleEffectRadius(effect.Radius2 != 0 ? effect.Radius2 : effect.Radius1, math);
        }

        private static string HandleEffectRadius(double radius, MathInfo math)
        {
            math.Apply(ref radius, true);
            return radius.ToTooltipNumber();
        }

        internal static string HandleEffectTime(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            IParserSpellEffect effect = parser.GetSpellEffect(spell, effectId);
            double time = effect.AuraPeriod;

            /* Workaround for bug in Blizzard's code */
            if (time == 0 && spell != baseSpell)
            {
                foreach (IParserSpellEffect effect2 in spell.Effects)
                {
                    if (effect2.TriggerSpellID != spell.InternalID || effect2.AuraPeriod == 0)
                        continue;

                    time = effect2.AuraPeriod;
                }
            }

            if (time == 0)
                time = 5000;

            math.Apply(ref time, false);

            // [WOW-402] Disabling this check fixes a variety of spells with one regression that is obviously a Blizzard bug.
            //if (!insideMath)
            time /= 1000;

            return time.ToString();
        }

        internal static string HandleGarrisonAbilityEffectDuration(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            var effect = parser.GetGarrisonAbilityEffect(effectId + 1);
            return effect?.ActionHours.ToString() ?? "0";
        }

        internal static string HandleGarrisonAbilityEffectPoints(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            var effect = parser.GetGarrisonAbilityEffect(effectId + 1);
            return effect?.ActionValueFlat.ToString() ?? "0";
        }

        internal static string HandleMaxApplications(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double result = spell.AuraOptions?.CumulativeAura ?? 0;
            math.Apply(ref result, true);
            return result.ToTooltipNumber();
        }
        internal static string HandleMaxTargetLevel(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double result = spell.TargetRestrictions?.MaxTargetLevel ?? 0;
            math.Apply(ref result, true);
            return result.ToTooltipNumber();
        }

        internal static string HandleMaxTargets(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double result = spell.TargetRestrictions?.MaxTargets ?? 0;
            math.Apply(ref result, true);
            return result.ToTooltipNumber();
        }

        internal static string HandleNewWeaponDamage(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            var effect = parser.GetSpellEffect(spell, effectId);

            // [WOW-295][WOW-481]
            // If a spell has a Weapon Damage SpellEffect and BasePoints of 0 or 1, scan the other effects looking for a WeaponPercentDamage to use.
            // Using the first effect seems to work fine with current spells, but may not in future :|
            if ((effect.Effect == SpellEffectEffect.WeaponDamage || effect.Effect == SpellEffectEffect.NormalizedWeaponDmg) && (effect.BasePoints == 0 || effect.BasePoints == 1))
            {
                IParserSpellEffect newEffect = parser.GetFirstWeaponPercentDamageSpellEffect(spell);
                if (newEffect != null)
                {
                    effect = newEffect;
                }
            }

            double points = 0, minPoints = 0, maxPoints = 0;
            effect.GetEffectPoints(out points, out minPoints, out maxPoints, options.ItemLevel, !insideMath, options.ItemSlot ?? 0,
                options.ItemRarity, options.PlayerLevel);

            if (points > 0)
            {
                return string.Format("{0}% of weapon damage", points);
            }
            else
            {
                // Fall back to normal effect points handling if weapon damage didn't pan out
                return HandleEffectPoints(parser, spell, baseSpell, effectId, insideMath, recursiveCall, math, options);
            }
        }

        internal static string HandlePlayerLevel(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            return options.PlayerLevel.ToString();
        }

        internal static string HandlePointsPerCombo(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double result = spell.GetEffectPointsPerCombo(effectId);
            math.Apply(ref result, true);
            return result.ToTooltipNumberAuto(!recursiveCall);
        }

        internal static string HandleProcChance(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double result = spell.AuraOptions != null ? spell.AuraOptions.ProcChance : 0;
            math.Apply(ref result, true);
            return result.ToTooltipNumberAuto(!recursiveCall);
        }

        internal static string HandleProcCharges(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double result = Math.Max(0, spell.AuraOptions?.ProcCharges ?? 0);
            math.Apply(ref result, false);
            return result.ToTooltipNumberAuto(!recursiveCall);
        }

        internal static string HandleProcCooldown(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double value = spell.AuraOptions?.ProcCategoryRecovery ?? 0;

            math.Apply(ref value, false);

            if (insideMath)
                return (value / 1000).ToString();
            else
                return TimeUtilities.GetTooltipTime((int)value, "??");
        }

        internal static string HandleProcPerMin(IParserSpell spell)
        {
            double value = spell.AuraOptions?.ProcsPerMin ?? 0;

            return String.Format(@"<span data-ppm=""{0}"">{0}</span>", value);
        }

        internal static string HandleProcPerMinDummy(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            return "$procrppm";
        }

        internal static string HandleRange(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double minRange = spell.Range?.MinRangeEnemies ?? 0;
            double maxRange = spell.Range?.MaxRangeEnemies ?? 0;

            math.Apply(ref minRange, true);
            math.Apply(ref maxRange, true);

            if (minRange == maxRange)
                return minRange.ToTooltipNumber();

            return String.Format("{0} to {1}", minRange.ToTooltipNumber(), maxRange.ToTooltipNumber());
        }

        internal static string HandleRangeMax(IBaseSpellParser parser, IParserSpell spell, IParserSpell baseSpell, int effectId, bool insideMath, bool recursiveCall, MathInfo math, SpellParserOptions options)
        {
            double maxRange = spell.Range?.MaxRangeEnemies ?? 0;

            math.Apply(ref maxRange, true);

            return maxRange.ToTooltipNumber();
        }

        private static void LogDebug(string text, params object[] args)
        {
#if DEBUG
            if (args.Length > 0)
                text = String.Format(text, args);
            using (var w = System.IO.File.AppendText(@"c:\temp\sdp_debug.txt"))
                w.WriteLine(text);
#endif
        }
    }
}
