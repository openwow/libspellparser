﻿using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.LibSpellParser
{
    public struct ParserRelatedSpell
    {
        public IParserSpell Spell;
        public IParserSpell RelatedSpell;
    }
}
