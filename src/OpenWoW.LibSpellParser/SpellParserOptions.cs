﻿using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.LibSpellParser
{
    public class SpellParserOptions
    {
        public bool DoSubTooltips = true;
        public bool RunningDiffs = false;
        public bool ShowIcons = true;
        public bool ShowMultiSpec = true;
        public bool ShowPerks = true;
        public int ActiveTree = 0;
        public int Build = 0;
        public int PlayerLevel = 0;
        public int PlayerMaxLevel = 0;
        public int SpecializationID = 0;
        public int? ArtifactRank = null;
        public int? ItemLevel = null;
        public int? ItemRarity = null;
        public int? ItemSlot = null;
        public Class? ItemClass = null;
        public SpellParserLog Log = null;

        public IEnumerable<int> SpecializationSpellIDs = null;

        public override string ToString()
        {
            return String.Join("|", new object[]
            {
                DoSubTooltips, RunningDiffs, ShowIcons, ShowMultiSpec, ShowPerks,
                ActiveTree, Build, PlayerLevel, PlayerMaxLevel, SpecializationID,
                ArtifactRank, ItemLevel, ItemRarity, ItemSlot,
                ItemClass,
            });
        }

        public SpellParserOptions()
        { }

        public SpellParserOptions(SpellParserOptions oldOptions)
        {
            this.ShowIcons = oldOptions.ShowIcons;
            this.ShowMultiSpec = oldOptions.ShowMultiSpec;
            this.ShowPerks = oldOptions.ShowPerks;
            this.ActiveTree = oldOptions.ActiveTree;
            this.Build = oldOptions.Build;
            this.PlayerLevel = oldOptions.PlayerLevel;
            this.PlayerMaxLevel = oldOptions.PlayerMaxLevel;
            this.SpecializationID = oldOptions.SpecializationID;
            this.ArtifactRank = oldOptions.ArtifactRank;
            this.ItemLevel = oldOptions.ItemLevel;
            this.ItemRarity = oldOptions.ItemRarity;
            this.ItemSlot = oldOptions.ItemSlot;
            this.ItemClass = oldOptions.ItemClass;
            this.Log = oldOptions.Log;
        }

        public void AddLog(string title, string description)
        {
            if (Log != null)
            {
                Log.Add(title, description);
            }
        }
    }
}
