﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.LibSpellParser
{
    public class SpellParserLog
    {
        public List<SpellParserLogEntry> Entries { get; private set; }

        public SpellParserLog()
        {
            this.Entries = new List<SpellParserLogEntry>();
        }

        public void Add(string title, string description)
        {
            // No reason to add another entry if nothing changed, reduces memory bloat and simplifies display
            if (Entries.Count > 0 && Entries.Last().Description == description)
            {
                return;
            }
            Entries.Add(new SpellParserLogEntry(title, description));
        }
    }

    public class SpellParserLogEntry
    {
        public string Title { get; private set; }
        public string Description { get; private set; }

        public SpellParserLogEntry(string title, string description)
        {
            this.Title = title;
            this.Description = description;
        }
    }
}
