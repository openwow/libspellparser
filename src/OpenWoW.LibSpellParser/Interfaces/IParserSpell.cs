﻿using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.LibSpellParser.Interfaces
{
    public interface IParserSpell
    {
        int ExternalID { get; }
        int InternalID { get; }

        string AuraDescription { get; }
        string Description { get; }
        string DescriptionCode { get; }
        string Name { get; }
        Class RequiredClass { get; }
        SpellFlags1 Flags1 { get; }
        SpellFlags2 Flags2 { get; }
        SpellFlags3 Flags3 { get; }
        SpellFlags4 Flags4 { get; }
        SpellFlags5 Flags5 { get; }
        SpellFlags6 Flags6 { get; }
        SpellFlags7 Flags7 { get; }
        SpellFlags8 Flags8 { get; }
        SpellFlags9 Flags9 { get; }
        SpellFlags10 Flags10 { get; }
        SpellFlags11 Flags11 { get; }

        SpellCategory Category { get; }
        int SubCategory { get; }
        int SubSubCategory { get; }
        int SubSubSubCategory { get; }

        IEnumerable<IParserSpell> AffectedByPerks { get; }
        IEnumerable<IParserChrSpecialization> ChrSpecializations { get; }
        IEnumerable<IParserSpellEffect> Effects { get; }
        IEnumerable<IParserSpellPower> Powers { get; }

        IParserSpellAuraOptions AuraOptions { get; }
        IParserSpellDuration Duration { get; }
        IParserSpellRange Range { get; }
        IParserSpellTargetRestrictions TargetRestrictions { get; }

        IEnumerable<IParserSpell> GetAffectedRelated(IParserSpellEffect effect);
        double GetArtifactEffectPoints(int rank);
        double GetEffectPointsPerCombo(int effectID);
        IParserSpell GetRelatedSpellWithClass();
    }
}
