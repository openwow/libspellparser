﻿using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.LibSpellParser.Interfaces
{
    public interface IParserSpellEffect
    {
        float Amplitude { get; }
        float BonusCoefficientFromAP { get; }
        float BonusCoefficientFromSP { get; }
        float ChainAmplitude { get; }
        float PointsPerLevel { get; }
        float Radius1 { get; }
        float Radius2 { get; }
        int AuraPeriod { get; }
        int BasePoints { get; }
        int ChainTargets { get; }
        int DifficultyID { get; }
        int Index { get; }
        int Misc1 { get; }
        int Misc2 { get; }
        int RandomPoints { get; }
        int TriggerSpellID { get; }
        SpellEffectAura Aura { get; }
        SpellEffectEffect Effect { get; }

        void GetEffectPoints(out double points, out double minPoints, out double maxPoints, int? itemLevel, bool v, int itemSlot, int? itemRarity, int? playerLevel);

        int GetModifierPoints(IEnumerable<int> spellIDs);
    }
}
