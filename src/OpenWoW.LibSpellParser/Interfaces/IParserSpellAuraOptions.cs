﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.LibSpellParser.Interfaces
{
    public interface IParserSpellAuraOptions
    {
        int CumulativeAura { get; }
        int ProcCategoryRecovery { get; }
        int ProcChance { get; }
        int ProcCharges { get; }
        float ProcsPerMin { get; }
    }
}
