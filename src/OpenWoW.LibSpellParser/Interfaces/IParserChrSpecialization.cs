﻿using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.LibSpellParser.Interfaces
{
    public interface IParserChrSpecialization
    {
        int ExternalID { get; }
        int OrderIndex { get; }
        string Name { get; }
        Class Class { get; }

        IEnumerable<int> SpecializationSpellIDs { get; }
        IEnumerable<int> SpecializationSpellPassiveInternalIDs { get; }
    }
}
