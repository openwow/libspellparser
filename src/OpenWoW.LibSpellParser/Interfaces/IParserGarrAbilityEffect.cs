﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.LibSpellParser.Interfaces
{
    public interface IParserGarrAbilityEffect
    {
        float ActionValueFlat { get; }
        int ActionHours { get; }
    }
}
