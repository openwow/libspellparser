﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OpenWoW.LibSpellParser.Interfaces;
using System.Threading;
using OpenWoW.Common;

namespace OpenWoW.LibSpellParser
{
    public abstract class BaseSpellParser : IBaseSpellParser
    {
        #region Abstract methods
        public abstract void HandleException(Exception e);
        public abstract IEnumerable<IParserChrSpecialization> GetAllChrSpecializationsByClass(Class spellClass);

        public abstract IParserChrSpecialization GetChrSpecialization(int specializationID);
        public abstract IParserGarrAbility GetGarrisonAbility(int abilityID);
        public abstract IParserGarrAbilityEffect GetGarrisonAbilityEffect(int effectID);
        public abstract IParserGarrBuilding GetGarrisonBuilding(int buildingID);
        public abstract IParserGarrBuilding GetGarrisonBuildingByNameAndLevel(string name, int level);
        public abstract IParserSpell GetNewSpell();
        public abstract IParserSpell GetSpell(int spellID);
        public abstract IParserSpell GetSpellByExternalID(int spellID);
        public abstract IParserSpellEffect GetSpellEffect(IParserSpell spell, int effectID);
        public abstract IParserSpellEffect GetFirstWeaponPercentDamageSpellEffect(IParserSpell spell);

        public abstract int GetScaledSecondaryStatValue(int value, int itemSlot, int itemLevel);
        public abstract string GetChrSpecializationIconURL(IParserChrSpecialization speicalization);
        public abstract string GetCurrencyLink(int currencyID, string text);
        public abstract string GetIconHtml(string iconName);
        public abstract string GetSpellIconHtml(IParserSpell spell);
        public abstract string GetSpellIconURL(IParserSpell spell);
        public abstract string RenderSpellTooltip(IParserSpell spell);
        #endregion

        private const int SPELL_PARSER_TIMEOUT = 20000; // Timeout in milliseconds

        public SpellParserOptions Options { get; private set; }

        public BaseSpellParser(SpellParserOptions options)
        {
            this.Options = options;
        }

        public string Parse(IParserGarrAbility ability)
        {
            return ParseGarrisonAbility(ability.Description);
        }

        public string Parse(IParserSpell spell, string description)
        {
            return InternalParse(spell, description);
        }

        public string ParseGarrisonAbility(string description)
        {
            // Strip colors first
            description = SpellDescriptionParser.colorRegex.Replace(description, delegate (Match match)
            {
                return match.Groups[2].Value;
            });
            // Replace $a with $ga so that we use the correct effect value
            description = description.Replace("$a", "$ga");
            // Replace $m with $ga so that we use the correct effect value - should this be MinPoints instead?
            description = description.Replace("$m", "$ga");
            // Replace $h with $gd so that we use the correct duration value
            description = description.Replace("$h", "$gd");

            return Parse(null, description);
        }

        public virtual string InternalParse(IParserSpell spell, string description)
        {
#if DEBUG
            string ret = null;
            var sdp = new SpellDescriptionParser(this, Options);
            Thread thread = new Thread(() => ret = sdp.Parse(spell, description));
            thread.IsBackground = true;
            thread.Start();
            bool finished = thread.Join(SPELL_PARSER_TIMEOUT);
            if (!finished)
            {
                thread.Abort();
                thread.Join();
                //ret = "Timeout while parsing!";
            }
            return ret;
#else
            var sdp = new SpellDescriptionParser(this, Options);
            return sdp.Parse(spell, description);
#endif
        }
    }
}
