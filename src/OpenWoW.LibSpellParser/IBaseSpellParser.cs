﻿using OpenWoW.Common;
using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.LibSpellParser
{
    public interface IBaseSpellParser
    {
        void HandleException(Exception e);
        IEnumerable<IParserChrSpecialization> GetAllChrSpecializationsByClass(Class spellClass);

        IParserChrSpecialization GetChrSpecialization(int specializationID);
        IParserGarrAbility GetGarrisonAbility(int abilityID);
        IParserGarrAbilityEffect GetGarrisonAbilityEffect(int effectID);
        IParserGarrBuilding GetGarrisonBuilding(int buildingID);
        IParserGarrBuilding GetGarrisonBuildingByNameAndLevel(string name, int level);
        IParserSpell GetNewSpell();
        IParserSpell GetSpell(int spellId);
        IParserSpell GetSpellByExternalID(int spellID);
        IParserSpellEffect GetFirstWeaponPercentDamageSpellEffect(IParserSpell spell);
        IParserSpellEffect GetSpellEffect(IParserSpell spell, int effectId);

        int GetScaledSecondaryStatValue(int value, int itemSlot, int itemLevel);
        string GetChrSpecializationIconURL(IParserChrSpecialization specialization);
        string GetCurrencyLink(int currencyID, string text);
        string GetIconHtml(string iconName);
        string GetSpellIconHtml(IParserSpell spell);
        string GetSpellIconURL(IParserSpell spell);
        string RenderSpellTooltip(IParserSpell spell);
    }
}
